import { CustomButton, Text } from "@/conponents"
import { projectsList } from "@/constant/data"
import useLogout from "@/containers/utils/hooks/logout"
import { AuthContext } from "@/context/user"
import Link from "next/link"
import { useRouter } from "next/router"
import { useContext } from "react"
import { Container, Form, Nav, Navbar } from "react-bootstrap"
import { FaHome, FaRegCompass, FaRegHeart, FaUserTie } from "react-icons/fa"
import { MdOutlineLogout } from "react-icons/md"
import { RiAddCircleLine, RiMessengerLine } from "react-icons/ri"

const Header = () => {
  const { logOut } = useLogout()
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
  const router: any = useRouter()
  console.log(router, 'routerrouter')
  return (
    <div className="mb-4 mt-2">
      {
        router == 'instagram' &&
        <Container className="transparentBg">
          {
            loggedInUser?.firstName &&
            <div className={`d-flex justify-content-between py-1`}>
              <div className="d-center">
                <Link href="/home">
                  <div className="rounded-circle me-2 bg-dark text-white px-1"><FaUserTie /></div>
                </Link>
                <Text textTransform="capitalize">
                  {loggedInUser?.firstName} {loggedInUser?.lastName}
                </Text>
              </div>
              <div className="d-flex gap-1">
                {
                  projectsList?.map((item: any, index: any) => {
                    return (
                      <Link key={index} href={item?.link} passHref>
                        <Text>{item?.projectName}</Text>
                      </Link>
                    )
                  })
                }
              </div>
              <CustomButton sizeVariant="CUSTOM" className="rounded-circle" label={<MdOutlineLogout />} onClick={() => logOut()} />
            </div>
          }
        </Container>
      }
    </div>
  )
}
export default Header