const apiEndPoint = {
   users: '/users',
   sendOtp: '/users/otp',
   userById: (id: any) => `/users/${id}`,
   login: '/users/login',
   // gallery
   image: '/image',
   getFolders: '/folders',
   folderById: (id: any) => `/folders/${id}`,
   getGallery: '/media',
   galleryById: (id: any) => `/media/${id}`,
   imageById: (id: any) => `/images/${id}`,
   imageFileUploader: (imgType: any) => `/fileuploader/img/?imgType=${imgType}`,
   getForm: '/generateform',
   formById: (id: any) => `/generateform/${id}`,
   // instagram
   getPosts: '/posts',
   postById: (id: any) => `/posts/${id}`,
   likePostById: (id: any) => `/posts/like/${id}`,
   unlikePostById: (id: any) => `/posts/unlike/${id}`,
   commentPostById: (id: any) => `/posts/comment/${id}`,
};
export default apiEndPoint;
