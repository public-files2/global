import { apiEndPoint } from '../handlers';
import api from '../handlers/api';

const get_images = async () => {
   const { data } = await api.get(apiEndPoint?.getGallery);
   return data;
};
const add_image = async (payload: any) => {
   const { data } = await api.post(apiEndPoint.getGallery, payload);
   return data;
};

const delete_image = async (id: any) => {
   const { data } = await api.delete(
      apiEndPoint.galleryById(id)
   );
   return data;
};
const update_image = async (value: any, id: any) => {
   const { data } = await api.patch(apiEndPoint.galleryById(id), value);
   return data;
};
export { get_images, add_image, delete_image, update_image };
