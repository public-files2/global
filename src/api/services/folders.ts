import { apiEndPoint } from '../handlers';
import api from '../handlers/api';

const getFolder = async ({ queryKey }: any) => {
   // if (!Array.isArray(queryKey)) {
   //    throw new Error('queryKey must be an array');
   // }
   const [_, userId, folderName] = queryKey;
   const params = {
      userId: userId,
      folderName: folderName,
   };

   const { data } = await api.get(apiEndPoint.getFolders, {
      params: params,
   });
   return data;
};

const addFolder = async (payload: any) => {
   const { data } = await api.post(apiEndPoint.getFolders, payload);
   return data;
};
const folderById = async (id: any) => {
   const { data } = await api.get(
      apiEndPoint.folderById(id)
   );
   return data;
};
const deleteFolder = async (id: any) => {
   const { data } = await api.delete(
      apiEndPoint.folderById(id)
   );
   return data;
};
const updateFolder = async (value: any, id: any) => {
   const { data } = await api.patch(apiEndPoint.folderById(id), value);
   return data;
};
export { getFolder, addFolder, deleteFolder, updateFolder, folderById };
