import { apiEndPoint } from '../handlers';
import api from '../handlers/api';

const getForm = async ({ queryKey }: any) => {
   // if (!Array.isArray(queryKey)) {
   //    throw new Error('queryKey must be an array');
   // }
   const [_, userId, formName] = queryKey;
   const params = {
      userId: userId,
      formName: formName,
   };

   const { data } = await api.get(apiEndPoint.getForm, {
      params: params,
   });
   return data;
};

const addForm = async (payload: any) => {
   const { data } = await api.post(apiEndPoint.getForm, payload);
   return data;
};
const formById = async (id: any) => {
   const { data } = await api.get(
      apiEndPoint.formById(id)
   );
   return data;
};
const deleteForm = async (id: any) => {
   const { data } = await api.delete(
      apiEndPoint.formById(id)
   );
   return data;
};
const updateForm = async (value: any, id: any) => {
   const { data } = await api.patch(apiEndPoint.formById(id), value);
   return data;
};
export { getForm, addForm, deleteForm, updateForm, formById };
