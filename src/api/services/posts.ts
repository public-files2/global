import { apiEndPoint } from '../handlers';
import api from '../handlers/api';
const getPostDetail = async ({ queryKey }: any) => {
   if (!Array.isArray(queryKey)) {
      throw new Error('queryKey must be an array');
   }
   const [_, folderId, userId, fileType] = queryKey;
   const params = {
      folderId: folderId,
      userId: userId,
      fileType: fileType,
   };

   const { data } = await api.get(apiEndPoint.getPosts, {
      params: params,
   });
   return data;
};

const addPost = async (payload: any) => {
   const { data } = await api.post(apiEndPoint.getPosts, payload);
   return data;
};

const deletePost = async (id: any) => {
   const { data } = await api.delete(
      apiEndPoint.postById(id)
   );
   return data;
};
const updatePost = async (value: any, id: any) => {
   const { data } = await api.patch(apiEndPoint.postById(id), value);
   return data;
};

const likePost = async (value: any, id: any) => {
   const { data } = await api.post(apiEndPoint.likePostById(id), value);
   return data;
};
const unlikePost = async (value: any, id: any) => {
   const { data } = await api.post(apiEndPoint.unlikePostById(id), value);
   return data;
};

const commentPost = async (value: any, id: any) => {
   const { data } = await api.post(apiEndPoint.commentPostById(id), value);
   return data;
};

const deleteComment = async (payload: any) => {
   const ids = `${payload?.postId}/${payload?.commentId}`
   const { data } = await api.delete(apiEndPoint.commentPostById(ids));
   return data;
};

export { getPostDetail, addPost, deletePost, updatePost, likePost, unlikePost, commentPost, deleteComment };
