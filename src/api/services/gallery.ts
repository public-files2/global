import { apiEndPoint } from '../handlers';
import api from '../handlers/api';
const getGalleryDetail = async ({ queryKey }: any) => {
   if (!Array.isArray(queryKey)) {
      throw new Error('queryKey must be an array');
   }
   const [_, folderId, userId, fileType] = queryKey;
   const params = {
      folderId: folderId,
      userId: userId,
      fileType: fileType,
   };

   const { data } = await api.get(apiEndPoint.getGallery, {
      params: params,
   });
   return data;
};
export { getGalleryDetail };
