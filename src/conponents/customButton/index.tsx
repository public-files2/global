import { useEffect, useState } from 'react';
import styles from './styles.module.css';
import { Button, Spinner } from 'react-bootstrap';

const CustomButton = (props: any) => {
  const {
    label,
    onClick,
    isLoading,
    className,
    sizeVariant = 'NORMAL',
    colorVariant = 'bg-one',
    fullWidth,
    endIcon,
    startIcon,
    startImg,
    endImg,
    disabled = false,
    edit
  } = props;

  const [fontSize, setFontSize] = useState(14);

  const handleResize = () => {
    const windowWidth = window.innerWidth;
    if (windowWidth < 768) {
      setFontSize(10);
    } else if (windowWidth < 1025) {
      setFontSize(12);
    } else {
      setFontSize(14);
    }
  };
  useEffect(() => {
    window.addEventListener('resize', handleResize);
    handleResize();
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const getBtnSize = (key: any) => {
    let classname = '';
    switch (key) {
      case 'NORMAL':
        classname = styles.normal;
        break;
      case 'SMALL':
        classname = styles.small;
        break;
      case 'CUSTOM':
        classname = styles.large;
        break;
      default:
        classname = styles.normal;
        break;
    }
    return classname;
  };
  const getBtnColor = (key: any) => {
    let classname = '';
    switch (key) {
      case 'bg-one':
        classname = styles.bg_one;
        break;
      default:
        classname = styles.bg_one;
        break;
    }
    return classname;
  };
  const style = {
    fontSize: fontSize,
    fontWeight: 700,
    lineHeight: 'initial',
    width: fullWidth && '100%',
  };

  return (
    <div className={`${styles.buttonsContainer}`}>
      <Button
        onClick={onClick}
        className={`${styles.shineliBtn} ${styles.btn} ${getBtnSize(
          sizeVariant
        )} ${getBtnColor(colorVariant)} ${className} position-relative my-auto d-center`}
        style={style}
        disabled={isLoading || disabled ? true : false}
      >
        {startIcon && !isLoading && (
          <span
            className={`${styles.zIndex3} me-1`}
            style={{ fontSize: '14px' }}
          >
            {startIcon}
          </span>
        )}
        {startImg && !isLoading && <img className="me-1" src={startImg} />}
        {isLoading ? (
          <Spinner animation="border" variant="info" />
        ) : (
          <span>{label} </span>
        )}
        {!edit &&
          <span>
            {endIcon && !isLoading && <img className="ms-2" src={endIcon} />}
            {endImg && !isLoading && <img className="ms-2" src={endImg} />}
          </span>
        }
        {edit &&
          <span className={`${styles.editBtn} rounded-circle`}>
            {<img src={'/assets/icon/edit.svg'} />}
          </span>
        }
      </Button>
    </div>
  );
};

export default CustomButton;
