import { Form } from 'react-bootstrap';
import styles from './styles.module.css';
import { CustomButton, Text } from '..';
import { useState } from 'react';
import { convertCamelCase } from '@/containers/utils/functions';

const TextField = (props: any) => {
  const {
    label = '',
    placeholder = '',
    disabled = false,
    readOnly = false,
    as,
    name,
    onChange,
    onClick,
    value,
    type = 'text',
    checked,
    style,
    className,
  } = props;
  const [checkValue, setCheckValue] = useState<any>()
  const handleChecks = (e: any) => {
    const { checked } = e.target
    console.log(checked)
    if (checked) {
      setCheckValue(convertCamelCase(name))
    } else {
      setCheckValue('')
    }
    e.target.value = checkValue
    onChange(e)
  }

  return (
    <div>
      <Form.Group className="mb-1 text-start">
        {
          (type != "checkbox" && type != "radio" && type != "button" && type != "submit") &&
          <>
            {label &&
              <Form.Label>
                <Text>
                  {label}
                </Text>
              </Form.Label>
            }
            <Form.Control
              size="sm"
              onChange={onChange}
              as={as}
              name={name}
              value={value}
              type={type}
              placeholder={placeholder}
              disabled={disabled}
              readOnly={readOnly}
              onClick={onClick}
              className={className}
              style={style}
            />
          </>
        }
        {
          (type == "checkbox" || type == "radio") &&
          <>
            {label &&
              <Form.Label>
                <Text>
                  {label}
                </Text>
              </Form.Label>
            }
            <Form.Check
              onChange={handleChecks}
              name={name}
              value={checkValue}
              type={type}
              checked={value}
              onClick={onClick}
            />
          </>
        }
        {
          (type == "button" || type == "submit") &&
          <div className='text-center mt-3'>
            <CustomButton
              onChange={onChange}
              label={name}
              type={type}
              onClick={onClick}
            />
          </div>
        }
      </Form.Group>
    </div>
  );
};

export default TextField;