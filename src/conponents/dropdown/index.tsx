import React from 'react';
import { Form } from 'react-bootstrap';
import { Text } from '..'; // Assuming Text component is defined in '..' (replace with actual path)

const DropdownField = (props: any) => {
  const {
    label = '',
    options = [],
    disabled = false,
    readOnly = false,
    name,
    onChange,
    value,
    style,
    className,
    optionName
  } = props;

  return (
    <div>
      <Form.Group className="mb-1 text-start">
        <Form.Label>
          <Text>{label}</Text>
        </Form.Label>
        <Form.Control
          as="select"
          size="sm"
          onChange={onChange}
          name={name}
          value={value}
          disabled={disabled}
          readOnly={readOnly}
          className={className}
          style={style}
        >
          <option>Select</option>
          {options.map((option: any, index: any) => (
            <option key={index} value={option.value}>
              {optionName ? option?.[optionName] : option}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
    </div>
  );
};

export default DropdownField;
