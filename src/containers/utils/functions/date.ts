export function convertDateFormat(dateString: any) {
   if (!dateString) {
      return '';
   }
   const inputDate = new Date(dateString);
   const year = inputDate.getFullYear();
   const month = ('0' + (inputDate.getMonth() + 1)).slice(-2);
   const day = ('0' + inputDate.getDate()).slice(-2);

   return `${day}/${month}/${year}`;
}

export function convertDateFormatToTime(dateString: any) {
   if (!dateString) {
      return '';
   }
   const date = new Date(dateString);

   // Extract hours, minutes, and seconds
   const hours = date.getHours();
   const minutes = date.getMinutes();
   const seconds = date.getSeconds();

   // Convert hours to 12-hour format
   let formattedHours = hours % 12;
   if (formattedHours === 0) {
      formattedHours = 12;
   }

   // Determine AM/PM
   const period = hours >= 12 ? 'PM' : 'AM';

   // Format minutes with leading zero if necessary
   const formattedMinutes = minutes < 10 ? `0${minutes}` : minutes;

   // Combine the formatted components
   const formattedTime = `${formattedHours}:${formattedMinutes} ${period}`;

   return formattedTime;
}

export const formatTimestamp = (createdAt: any) => {
   const date: any = new Date(createdAt ?? '2023-11-23T15:02:55.000Z');
   let year = new Intl.DateTimeFormat('en', {
      year: 'numeric',
      timeZone: 'UTC',
   }).format(date);
   let month = new Intl.DateTimeFormat('en', {
      month: 'long',
      timeZone: 'UTC',
   }).format(date);
   let day = new Intl.DateTimeFormat('en', {
      day: '2-digit',
      timeZone: 'UTC',
   }).format(date);

   const hours = String(date.getUTCHours() % 12 || 12).padStart(2, '0');
   const minutes = String(date.getUTCMinutes()).padStart(2, '0');
   const period = date.getUTCHours() >= 12 ? 'PM' : 'AM';
   return `${month} ${day} ${year}, ${hours}:${minutes} ${period}`;
};
