import { getUserById } from "@/api/services/user";

export const updateLoggedInUser = (loggedInUser: any, setLoggedInUser: any) => {
   const fetchData = async () => {
      const userData = await getUserById(loggedInUser?._id);
      console.log(userData, 'ggggg')
      if (userData?.data) {
         setLoggedInUser(userData?.data);
         localStorage.setItem('LOGGED_USER', JSON.stringify(userData?.result));
      }
   };
   fetchData();
};
