const regex = {
   email: /^(?:[\w!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/,
   pinCode: /^[1-9][0-9]{5}$/,
   password: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/,
   percent: /^[1-9][0-9]?$|^100$/,
   onlyNumber: /^[1-9]+[0-9]*$/,
   onlyAlphabet: /^[A-Za-z ]+$/,
};

// <-------- validate Phone ---------->
export const validatePhone = (phone: any) => {
   if (phone?.toString().length < 1) {
      return false;
   } else if (
      phone?.toString().length >= 10 &&
      phone?.toString().length <= 12 &&
      regex.onlyNumber.test(phone)
   ) {
      return true;
   } else {
      return false;
   }
};

// <----- validate email ------------->
export const validateEmail = (email: any) => {
   if (email && email.length < 1) {
      return false;
   } else if (email && regex.email.test(email)) {
      return true;
   } else {
      return false;
   }
};