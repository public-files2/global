import { AuthContext } from "@/context/user";
import { useRouter } from "next/router";
import { useContext } from "react";

const useLogout = () => {
    const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
    const router = useRouter()
    const logOut = () => {
        localStorage.removeItem('LOGGED_USER');
        setLoggedInUser(null)
        router.push('/login')
    }
    return {
        logOut
    }
}
export default useLogout