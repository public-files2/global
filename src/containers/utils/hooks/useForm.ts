import { useCallback, useState } from 'react';

export function useForm() {
	const [initialState] = useState('')
	const [form, setForm] = useState<any>(initialState);

	const handleChange = (event: any) => {
		let title = event.target.name
		let value = event.target.value
		setForm({ ...form, [title]: value })
	}

	const resetForm = useCallback(() => {
		setForm(initialState);
	}, [form, initialState]);

	const setInForm = useCallback((name: any, value: any) => {
		// setForm((_form: any) => _.setIn(_form, name, value));
	}, []);


	return {
		form,
		handleChange,
		resetForm,
		setForm,
		setInForm
	};
}
