import { get_image_url } from '@/api/services/upload';
import { useMutation } from '@tanstack/react-query';
import { useState } from 'react';

const useFileUpload = () => {
   const [imageUrl, setImageUrl] = useState<any>('');

   const { mutate: addImageUpload, isLoading: imageUrlLoader }: any = useMutation({
      mutationFn: (payload: any) => get_image_url(payload),
      onSuccess: async ({ data }: any) => {
         if (data) {
            setImageUrl(data?.imgUrl);
         }
      },
      onError: (data: any) => { },
   });
   const callImageUpload = (formData: any, imgType: string) => {
      addImageUpload({ formData, imgType });
   };
   // console.log(imageUrlLoader, 'hhh')
   return {
      imageUrl,
      callImageUpload,
      setImageUrl,
   };
};

export default useFileUpload;