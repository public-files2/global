
import { AuthContext } from "@/context/user";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { addForm, deleteForm, getForm, updateForm } from "@/api/services/formGen";
import { useForm } from "@/containers/utils/hooks/useForm";

const useFormHook = () => {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
  const { form, setForm, handleChange } = useForm();

  const { data, refetch } = useQuery({
    queryKey: ["FORMS", loggedInUser?._id],
    queryFn: getForm,
    enabled: !!loggedInUser?._id,
  });

  return {
    loggedInUser,
    data,
    form,
    setForm,
    handleChange
  }
}
export default useFormHook