import React, { useState } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import useFormHook from "./hooks";
import GeneratorForm from "./generatedForm";
import { CustomButton } from "@/conponents";
import Link from "next/link";

const GeneratedForms = () => {

  const {
    data,
    form,
    handleChange
  } = useFormHook()


  console.log("Form Data:", data);

  return (
    <Container className="transparentBg p-1">
      <Link href='/formgenerator' passHref>
        <CustomButton label="Generate Form" />
      </Link>
      <div className="">
        <Row className='justify-content-center'>
          {
            data?.data?.map((item: any, index: any) => {
              return (
                <Col md={6} key={index}>
                  <div className="text-center mb-3">
                    <h2>{item?.formName}</h2>
                  </div>
                  <GeneratorForm
                    handleChange={handleChange}
                    form={form}
                    items={item}
                  />
                </Col>
              )
            })
          }
        </Row>
      </div>
    </Container>
  );
};

export default GeneratedForms;
