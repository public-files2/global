import { CustomButton, TextField } from "@/conponents";
import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";
import { convertCamelCase } from "@/containers/utils/functions";

const GeneratorForm = (props: any) => {
  const {
    handleChange,
    form,
    items,
  } = props

  return (
    <div className="transparentBg p-1">
      <Row>
        {items?.formDetails?.map((item: any, index: any) => (
          <Col md={item?.gridNumber ? item?.gridNumber : 12} key={index}>
            <TextField
              label={item?.label}
              name={item?.valueName ? convertCamelCase(item?.valueName) : convertCamelCase(item?.label)}
              value={form?.[item?.name]}
              type={item?.inputType ? item?.inputType : 'text'}
              onChange={handleChange}
            />
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default GeneratorForm;
