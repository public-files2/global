import { Col, Row } from "react-bootstrap"
import styles from "../styles.module.css"
import { CustomButton, Text, TextField } from "@/conponents"
const SignUpOtp = (props: any) => {
  const {
    setScreenType,
    handlechange,
    form,
    handleSignUp,
    handleSendOtp,
  } = props
  return (
    <>
      <div className={`${styles.wrapper}`}>
        <Row action="">
          <h1>OTP verification</h1>
          <div>
            <Text>OTP sent on email <Text>{form?.email}</Text></Text>
          </div>
          <Col md={6}>
            <TextField onChange={handlechange} name="otp" value={form?.otp && form?.otp} type="number" placeholder="Enter OTP" required />
          </Col>
          <div className="text-center mt-2">
            <CustomButton label="Submit"
              onClick={() => { handleSignUp() }}
              disabled={form?.otp ? false : true}
            />
            <div role='button' className={`${styles.register_link}`} onClick={(e) => handleSendOtp()}>
              resend
            </div>
          </div>
        </Row>
      </div>
    </>
  )
}
export default SignUpOtp