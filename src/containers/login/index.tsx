import useLoginHook from "./hooks"
import SignUp from "./signUp"
import Link from "next/link"
import styles from "./styles.module.css"
import { Col, Container, Row } from "react-bootstrap"
import { CustomButton, Text, TextField } from "@/conponents"
import SignUpOtp from "./otp"

const Login = (props: any) => {

  const {
    screenType,
    setScreenType,
    handlechange,
    handleSignUp,
    onLogin,
    form,
    handleSendOtp,
  } = useLoginHook()
  return (
    <Container fluid style={{ height: '100vh' }} className="d-center">
      {
        screenType === "login" &&
        <Row className={`${styles.wrapper}`}>
          <h1>Login</h1>
          <Col md={12}>
            <TextField onChange={handlechange} name="userName" value={form?.userName && form?.userName} type="text" placeholder="userName" required />
          </Col >
          <Col md={12}>
            <TextField onChange={handlechange} name="password" value={form?.password && form?.password} type="text" placeholder="Password" required />
          </Col>
          <Col md={12} className={`my-3 d-flex justify-content-between`}>
            <Text size={14}>Remember me</Text>
            <Link href="#">
              <Text size={14}>Forgot password?</Text>
            </Link>
          </Col>
          <div className="text-center">
            <CustomButton label="Login" onClick={() => onLogin()} className={`text-bold`} />
            <div className={`${styles.register_link}`} onClick={() => setScreenType('signup')}>
              {`Dont't have an account?`}
              <p>Register</p>
            </div>
          </div>
        </Row>
      }
      {
        screenType == "signup" &&
        <SignUp
          handleSendOtp={handleSendOtp}
          handlechange={handlechange}
          form={form}
          screenType={screenType}
          setScreenType={setScreenType}
        />
      }
      {
        screenType == "otp" &&
        <SignUpOtp
          handleSignUp={handleSignUp}
          handlechange={handlechange}
          handleSendOtp={handleSendOtp}
          form={form}
          screenType={screenType}
          setScreenType={setScreenType}
        />
      }
    </Container>
  )
}
export default Login