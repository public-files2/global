import { Col, Row } from "react-bootstrap"
import styles from "../styles.module.css"
import { CustomButton, TextField } from "@/conponents"
const SignUp = (props: any) => {
  const {
    setScreenType,
    handlechange,
    form,
    handleSendOtp,
  } = props
  return (
    <>
      <div className={`${styles.wrapper}`}>
        <Row action="">
          <h1>Sign Up</h1>
          <Col md={6} >
            <TextField onChange={handlechange} name="firstName" value={form?.firstName && form?.firstName} type="text" placeholder="First Name" required />
          </Col>
          <Col md={6} >
            <TextField onChange={handlechange} name="lastName" value={form?.lastName && form?.lastName} type="text" placeholder="Last Name" required />
          </Col>
          <Col md={12} >
            <TextField onChange={handlechange} name="email" value={form?.email && form?.email} type="text" placeholder="Email" required />
          </Col>
          <Col md={12} >
            <TextField onChange={handlechange} name="userName" value={form?.userName && form?.userName} type="text" placeholder="userName" required />
          </Col>
          <Col >
            <TextField onChange={handlechange} name="password" value={form?.password && form?.password} type="text" placeholder="Password" required />
          </Col>
          <div className="text-center mt-2">
            <CustomButton label="Submit" onClick={() => { handleSendOtp() }} />
            <div role='button' className={`${styles.register_link}`} onClick={(e) => setScreenType('login')}>
              Already have an account?
              <p>Login</p>
            </div>
          </div>
        </Row>
      </div>
    </>
  )
}
export default SignUp