
import { KEYS } from "@/api/handlers";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import useFileUpload from "@/containers/utils/hooks/useFileUpload";
import { AuthContext } from "@/context/user";
import { useForm } from "@/containers/utils/hooks/useForm";
import { addPost, commentPost, deleteComment, deletePost, getPostDetail, likePost, unlikePost } from "@/api/services/posts";

const usePostHook = () => {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
  const { form, setForm, handleChange: handleChange } = useForm();
  const { imageUrl, callImageUpload, setImageUrl } = useFileUpload();
  const [addImgModalShow, setAddImgModalShow] = useState(false);

  const closeImgModal = () => {
    setAddImgModalShow(false)
    setForm({ ...form, url: '' })
  }

  useEffect(() => {
    if (imageUrl) {
      setForm({ ...form, url: imageUrl });
    }
  }, [imageUrl]);

  const handleDrop = (acceptedFiles: any) => {
    const file = acceptedFiles;
    if (file) {
      const formData = new FormData();
      formData.append('image', file);
      callImageUpload(formData, 'gallery');
    }
  };

  const clearForm = () => {
    setForm({ ...form, url: '', title: '', comment: '' })
  }
  useEffect(() => {
    setForm({ ...form, userId: loggedInUser?._id })
  }, [form.userId])

  const { data, refetch } = useQuery({
    queryKey: [KEYS.post],
    queryFn: getPostDetail,
  });

  const addMutation = useMutation({
    mutationFn: (payload) => addPost(payload),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      refetch()
      toast.success("image added")
      closeImgModal()
      setImageUrl('')
      clearForm()
    },
  });
  const handleAddData = () => {
    let finalData: any = {
      url: form?.url,
      title: form?.title,
      userId: loggedInUser?._id || form?.userId
    }

    if (finalData?.url) {
      if (finalData?.userId) {
        addMutation.mutate(finalData);
      } else {
        toast.warning("Something went wrong")
      }
    } else {
      toast.warning("select a image first")
    }
  }
  const addLikes = useMutation({
    mutationFn: (payload: any) => likePost(payload, payload?.postId),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      refetch()
      toast.success("like")
      closeImgModal()
      setImageUrl('')
      clearForm()
    },
  });
  const handleLikes = (id: any) => {
    let finalData: any = {
      userId: loggedInUser?._id || form?.userId,
      postId: id
    }
    if (finalData?.userId) {
      addLikes.mutate(finalData, id);
    } else {
      toast.warning("Something went wrong")
    }
  }
  const removeLikes = useMutation({
    mutationFn: (payload: any) => unlikePost(payload, payload?.postId),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      refetch()
      toast.success("like removed")
      closeImgModal()
      setImageUrl('')
      clearForm()
    },
  });
  const handleRemoveLikes = (id: any) => {
    let finalData: any = {
      userId: loggedInUser?._id || form?.userId,
      postId: id
    }
    if (finalData?.userId) {
      removeLikes.mutate(finalData);
    } else {
      toast.warning("Something went wrong")
    }
  }
  const addComments = useMutation({
    mutationFn: (payload: any) => commentPost(payload, payload?.postId),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      refetch()
      toast.success("comment added")
      closeImgModal()
      setImageUrl('')
      clearForm()
    },
  });
  const handleComments = (id: any) => {
    let finalData: any = {
      userId: loggedInUser?._id || form?.userId,
      text: form?.comment,
      postId: id,
    }
    if (finalData?.userId && form?.comment) {
      addComments.mutate(finalData);
    } else {
      toast.warning("Add a comment")
    }
  }
  const removeComments = useMutation({
    mutationFn: (payload: any) => deleteComment(payload),
    onError: (error) => {
      console.log(error, `rolling back optimistic update`);
    },
    onSuccess: (data) => {
      refetch()
      toast.error("Image deleted")
      clearForm()
    },
  });
  const handleDeleteComment = (postId: any, commentId: any) => {
    const data = {
      postId: postId,
      commentId: commentId
    }
    removeComments.mutate(data)
  }
  const deleteMutation = useMutation({
    mutationFn: (id) => deletePost(id),
    onError: (error) => {
      console.log(error, `rolling back optimistic update`);
    },
    onSuccess: (data) => {
      refetch()
      toast.error("Image deleted")
      clearForm()
    },
  });
  const handleDelete = (id: any) => {
    deleteMutation.mutate(id)
  }

  return {
    handleAddData,
    handleDelete,
    data,
    handleChange,
    addImgModalShow,
    setAddImgModalShow,
    closeImgModal,
    form,
    handleDrop,
    handleLikes,
    handleRemoveLikes,
    handleComments,
    handleDeleteComment,
  }
}
export default usePostHook