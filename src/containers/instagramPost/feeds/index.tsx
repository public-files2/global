import { StatusUI, Text, TextField } from "@/conponents";
import { AuthContext } from "@/context/user";
import { PiTagChevron } from "react-icons/pi";
import { useContext } from "react";
import { Col, Image, Row } from "react-bootstrap"
import { FaHeart, FaRegHeart, FaUserTie } from "react-icons/fa";
import { RiMessengerLine } from "react-icons/ri";
import { TbDots } from "react-icons/tb";
import { LuSend, LuSmile } from "react-icons/lu";
import { MdDeleteSweep } from "react-icons/md";

const Feeds = (props: any) => {
  const { loggedInUser } = useContext(AuthContext);
  const {
    items,
    handleLikes,
    handleRemoveLikes,
    handleComments,
    form,
    handleChange,
    handleDeleteComment,
    handleDelete
  } = props

  const filterUsers = () => {
    let match = items?.likes?.filter((value: any) => value == loggedInUser?._id)
    if (match.length > 0) {
      return true
    } else {
      return false
    }
  }
  const userAuthorization = (id: any) => {
    if (id == loggedInUser?._id) {
      return true
    } else {
      return false
    }
  }

  return (
    <div className="d-center my-4">
      <div className="bg-white w-75 p-2 rounded">
        <div className="d-center justify-content-between">
          <div >
            <div className="d-flex align-items-center gap-1">
              <StatusUI src={items?.url} width={25} height={25} />
              <div className="d-flex flex-column">
                <Text size={11} textTransform="">
                  {items?.userId?.firstName} {items?.userId?.lastName}
                </Text>
                <Text size={10} textTransform="">
                  {items?.userId?.userName}
                </Text>
              </div>
            </div>
          </div>
          <div className="text-end position-relative">
            <div className="d-center">
              {
                userAuthorization(items?.userId?._id) &&
                <div role="button" onClick={() => handleDelete(items?._id)} className="d-flex position-absolut top-0 end-0">
                  <MdDeleteSweep />
                </div>
              }
              <Text>
                <TbDots />
              </Text>
            </div>
          </div>
        </div>
        <Row className="justify-content-start">
          <Col xs={12}>
            <Image className="rounded-2 mt-4" src={items?.url} width="100%" height="250px" alt='image' />
          </Col>
          <Col md={12}>
            <div className="d-center justify-content-between mt-3">
              <div className="d-center gap-2">
                <div role="button" onClick={() => filterUsers() ? handleRemoveLikes(items?._id) : handleLikes(items?._id)}>
                  <Text size={20}>{filterUsers() ? <FaHeart /> : <FaRegHeart />}</Text>
                </div>
                <Text size={20}><RiMessengerLine /></Text>
                <Text size={20}><LuSend /></Text>
              </div>
              <div>
                <div style={{ transform: "rotate(-90deg)" }}>
                  <Text size={21}><PiTagChevron /></Text>
                </div>
              </div>
            </div>
            <div><Text size={10}>{items?.likes.length} people likes</Text></div>
          </Col>
          <Col xs={12}>
            <Text>{items?.title}</Text>
          </Col>
          <Col xs={12} className="mb-2">
            <div>
              {
                items?.comments?.map((comment: any, key: any) => {
                  return (
                    <div className="d-flex gap-2 align-items-start mt-2 position-relative">
                      <div className="d-center">
                        <StatusUI src={items?.url} width={25} height={25} />
                      </div>
                      <div className="d-flex flex-column mt-1">
                        <Text size={10} textTransform="">
                          {comment?.userId?.userName}
                        </Text>
                        <Text>{comment?.text}</Text>
                      </div>
                      {
                        userAuthorization(comment?.userId?._id) &&
                        <div role="button" onClick={() => handleDeleteComment(items?._id, comment?._id)} className="d-flex position-absolute top-0 end-0">
                          <MdDeleteSweep />
                        </div>
                      }
                    </div>
                  )
                })
              }
            </div>
          </Col>
          <Col xs={12} className="border-top">
            <div className="d-center justify-content-between my-3">
              <Text size={21}><LuSmile /></Text>
              <TextField className='border-0' name="comment" value={form?.comment && form?.comment} onChange={handleChange} placeholder='Add a comment...' />
              <div role="button" onClick={() => handleComments(items?._id)}>
                <Text>Post</Text>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  )
}
export default Feeds