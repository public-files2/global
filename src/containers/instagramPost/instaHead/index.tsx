import { CustomButton, Text } from "@/conponents"
import { projectsList } from "@/constant/data"
import useLogout from "@/containers/utils/hooks/logout"
import { AuthContext } from "@/context/user"
import Link from "next/link"
import { useContext } from "react"
import { Container, Form, Nav, Navbar } from "react-bootstrap"
import { FaHome, FaRegCompass, FaRegHeart, FaUserTie } from "react-icons/fa"
import { MdOutlineLogout } from "react-icons/md"
import { RiAddCircleLine, RiMessengerLine } from "react-icons/ri"

const InstaHead = (props: any) => {
  const {
    setAddImgModalShow,
    showInsta
  } = props;
  const { logOut } = useLogout()
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);

  return (
    <div className="mb-4 mt-2">
      <Navbar collapseOnSelect expand="lg" className="bg-body-tertiary">
        <Container>
          <Navbar.Brand href="#home"><Text>Instagram</Text></Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto">
              <Form className="d-flex">
                <Form.Control
                  size="sm"
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                />
              </Form>
            </Nav>
            <div className="d-center gap-2">
              <Nav.Link href="#"><Text size={20}><FaHome /></Text></Nav.Link>
              <Nav.Link href=""><div onClick={() => setAddImgModalShow(true)}><Text size={20}><RiAddCircleLine /></Text></div></Nav.Link>
              <Nav.Link href="#"><Text size={20}><FaRegCompass /></Text></Nav.Link>
              <Nav.Link href="#"><Text size={20}><FaRegHeart /></Text></Nav.Link>
              <Nav.Link href="#"><Text size={20}><RiMessengerLine /></Text></Nav.Link>
              <Link href="/home">
                <div className="rounded-circle me-2 bg-dark text-white px-1">
                  <FaUserTie />
                </div>
              </Link>
              <CustomButton sizeVariant="CUSTOM" className="rounded-circle" label={<MdOutlineLogout />} onClick={() => logOut()} />
            </div>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}
export default InstaHead