import { StatusUI, Text } from "@/conponents";
import styles from '../styles.module.css'

const PeopleStatus = () => {
  return (
    <div className={`${styles.statusWrapper}`}>
      <div className="d-center gap-2">
        {
          [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,].map((item: any, index: any) => {
            return (
              <div key={index}>
                <StatusUI label="_abhi" src="/assets/camera.webp" width={50} height={50} />
              </div>
            )
          })
        }
      </div>
    </div>
  )
}
export default PeopleStatus