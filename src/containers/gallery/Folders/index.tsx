import { Button, Col, Container, Row } from "react-bootstrap";
import useFolderHook from "./hooks";
import { useState } from "react";
import { CustomButton, Text } from "@/conponents";
import FolderList from "./folderList";
import AddFolder from "./addFolder";
import styles from "./styles.module.css"
import Link from "next/link";
import { MdEdit } from "react-icons/md";

const Gallery2 = (props: any) => {
  const {
    addFolderModalShow,
    setAddFolderModalShow,
    form,
    data,
    setForm,
    handleChange,
    loggedInUser,
    handleUpdate,
    handleAddData,
    handleEdit,
    handleDelete,
    closeFolderModel,
  } = useFolderHook()

  return (
    <Container className="transparentBg">
      <div style={{ minHeight: '80vh' }} className={`${data?.data?.length <= 0 && "d-center"}`}>
        {
          data?.data?.length > 0 ?

            <Row className='h-100'>
              {data?.data?.map((item: any, index: any) => {
                return (
                  <Col xs={3} key={index} className='mb-3' style={{ cursor: "pointer" }}>
                    <div className={`mt-5 folderWrapper scaleItems bg-tr rounded shadow position-relative`}>
                      <Link href={{ pathname: '/gallery/images', query: { folderId: item?._id, userId: loggedInUser?._id || item?.userId } }} passHref>
                        <FolderList
                          onClick={() => { }}
                          className="d-center flex-column"
                          size={88}
                          item={item}
                          type="gridView"
                        />
                      </Link>
                      <div onClick={() => handleEdit(item)} className="folderEditBtn position-absolute bg-warning p-1 m-1 d-center rounded-circle top-0 end-0">
                        <MdEdit />
                      </div>
                    </div>
                  </Col>
                );
              })}
            </Row>
            :
            <div className="d-center flex-column" >
              <Text>Add a folder to start uploading your pictures</Text>
              <CustomButton
                label="Add"
                // sizeVariant="CUSTOM"
                onClick={() => setAddFolderModalShow(true)}
                className='mt-4'
              />
            </div>
        }
        <AddFolder
          handleChange={handleChange}
          setForm={setForm}
          form={form}
          addFolderModalShow={addFolderModalShow}
          setAddFolderModalShow={setAddFolderModalShow}
          handleUpdate={handleUpdate}
          handleAddData={handleAddData}
          handleDelete={handleDelete}
          closeFolderModel={closeFolderModel}
        />
        <div className="">
          <CustomButton
            label="+"
            sizeVariant="CUSTOM"
            onClick={() => setAddFolderModalShow(true)}
            className='rounded-circle fs-5 position-fixed pt-1 bottom-0 end-0 mb-2 me-3'
          />
        </div>
      </div>
    </Container >
  )
}
export default Gallery2