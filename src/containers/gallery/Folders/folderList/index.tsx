import { Text } from '@/conponents';
import { colors } from '@/constant';
import { FaFolderOpen } from 'react-icons/fa';

const FolderList = (props: any) => {
  const { item, size, className, onClick } = props

  return (
    <div className={`mt-5 ${className}  rounded shadow`}>
      <div onClick={onClick} className='d-center w-100 flex-column'>
        <Text size={size} color={colors?.colorfour} className="">
          <FaFolderOpen />
        </Text>
        <Text size={16} color={colors?.colorfour} className="">
          {item?.folderName}
        </Text>
      </div>
    </div>
  )
}
export default FolderList