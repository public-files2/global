
import { AuthContext } from "@/context/user";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { addFolder, deleteFolder, getFolder, updateFolder } from "@/api/services/folders";
import { useForm } from "@/containers/utils/hooks/useForm";

const useFolderHook = () => {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
  const { form, setForm, handleChange } = useForm();
  const [addFolderModalShow, setAddFolderModalShow] = useState(false);
  const closeFolderModel = () => {
    clearForm()
    setForm({ ...form, isEdit: '', folderName: '', editId: '' })
    setAddFolderModalShow(false)
  }
  useEffect(() => {
    setForm({ ...form, userId: loggedInUser?._id })
  }, [form?.userId])

  const { data: data, refetch } = useQuery({
    queryKey: ["FOLDERS", loggedInUser?._id],
    queryFn: getFolder,
    enabled: !!loggedInUser?._id,
  });

  const addMutation = useMutation({
    mutationFn: (payload) => addFolder(payload),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      refetch()
      toast.success("folder added")
      setAddFolderModalShow(false)
    },
  });

  const handleAddData = () => {
    let finalData: any = {
      folderName: form?.folderName,
      userId: loggedInUser?._id || form?.userId
    }
    if (finalData?.folderName) {
      if (finalData?.userId) {
        addMutation.mutate(finalData);
      } else {
        toast.warning("Something went wrong")
      }
    } else {
      toast.warning("please enter folder Name")
    }
  }

  const clearForm = () => {
    setForm({ ...form, editId: '', folderName: '' })
  }

  const deleteMutation = useMutation({
    mutationFn: (id) => deleteFolder(id),
    onError: (error) => {
      console.log(error, `rolling back optimistic update`);
    },
    onSuccess: (data) => {
      refetch()
      setAddFolderModalShow(false)
      toast.error("Folder deleted")
    },
  });
  const handleDelete = (id: any) => {
    deleteMutation.mutate(id)
  }

  const handleEdit = (data: any) => {
    setForm({ ...form, isEdit: true, folderName: data?.folderName, editId: data?._id })
    setAddFolderModalShow(true)
  }

  const updateMutation = useMutation({
    mutationFn: (payload) => updateFolder(payload, form?.editId),
    onError: (error: any) => {
      console.log(error, `rolling back optimistic update`);
    },
    onSuccess: (data) => {
      refetch()
      closeFolderModel()
      toast.success("folder updated")
    },
  });
  const handleUpdate = (data: any) => {
    let finalData: any = {
      folderName: form?.folderName
    }
    console.log(finalData, 'finalData')
    updateMutation.mutate(finalData)
  }
  return {
    handleDelete,
    handleUpdate,
    handleAddData,
    handleChange,
    loggedInUser,
    form,
    data,
    setForm,
    addFolderModalShow,
    setAddFolderModalShow,
    handleEdit,
    closeFolderModel
  }
}
export default useFolderHook