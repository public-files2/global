import { CustomButton, Text, TextField } from "@/conponents"
import { Form, Modal } from "react-bootstrap"
import { MdDelete } from "react-icons/md"

const AddFolder = (props: any) => {
  const {
    handleChange,
    form,
    addFolderModalShow,
    setAddFolderModalShow,
    handleUpdate,
    handleAddData,
    handleDelete,
    closeFolderModel,
  } = props

  return (
    <div className="d-center" >
      <Modal
        show={addFolderModalShow} onHide={() => closeFolderModel()}
        size='sm'
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <Text>{form?.isEdit == true ? "Modify" : "Add New Image"}</Text>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            form?.editId &&
            <div className="d-flex justify-content-end">
              <div onClick={() => handleDelete(form?.editId)} className="bg-dark text-white p-1 d-center rounded-circle">
                <MdDelete />
              </div>
            </div>
          }
          <div className="text-center">
            <TextField label='Folder Name' onChange={handleChange} name='folderName' value={form?.folderName && form?.folderName} />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div className="p-2 d-center gap-1 justify-content-end" >
            <CustomButton
              sizeVariant="SMALL"
              label="Cancel"
              onClick={() => closeFolderModel()}
            />
            <CustomButton
              onClick={() => form?.isEdit == true ? handleUpdate() : handleAddData()}
              label={form?.isEdit == true ? "Upadte" : "Add"}
              sizeVariant="SMALL"
            />
          </div>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
export default AddFolder