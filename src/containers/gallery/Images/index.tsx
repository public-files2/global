import { Container } from "react-bootstrap";
import ListView from "./listView";
import useGalleryHook from "./hooks";
import AddImage from "./addImage";
import GridView from "./gridView";
import { useState } from "react";
import { CustomButton, Text } from "@/conponents";
import ViewImage from "./viewImage";
import styles from "./styles.module.css"
import Link from "next/link";
import { IoArrowBackOutline } from "react-icons/io5";

const GalleryImages = (props: any) => {
  const [viewType, setViewType] = useState("grid");

  const {
    handleAddData,
    handleDelete,
    handleEdit,
    data,
    handleChange,
    addImgModalShow,
    setAddImgModalShow,
    closeImgModal,
    viewImgShow,
    setViewImgShow,
    form,
    setForm,
    loggedInUser,
    handleDrop,
  } = useGalleryHook()

  return (
    <Container className="">
      <div style={{ minHeight: '80vh' }}>
        <div className="d-center justify-content-between px-2">
          <Link href={'/'}>
            <CustomButton
              label={<IoArrowBackOutline />}
              sizeVariant="CUSTOM"
            />
          </Link>
          <div className={`d-center`}>
            <div className={`d-center transparentBg`}>
              <div className={`${styles.viewTypeBtn} ${viewType == "grid" && styles.viewBtnActive}`} role="button" onClick={() => setViewType("grid")}>
                <Text>Grid</Text>
              </div>
              <div className={`${styles.viewTypeBtn} ${viewType == "list" && styles.viewBtnActive}`} role="button" onClick={() => setViewType("list")}>
                <Text>List</Text>
              </div>
            </div>
          </div>
        </div>
        {
          viewType == "grid" &&
          <GridView
            setForm={setForm}
            form={form}
            loggedInUser={loggedInUser}
            data={data?.data}
            handleEdit={handleEdit}
            setViewImgShow={setViewImgShow}
          />

        }
        {
          viewType == "list" &&
          <ListView
            setForm={setForm}
            form={form}
            loggedInUser={loggedInUser}
            data={data?.data}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
            setViewImgShow={setViewImgShow}
          />
        }
        <AddImage
          handleAddData={handleAddData}
          form={form}
          addImgModalShow={addImgModalShow}
          closeImgModal={closeImgModal}
          handleDrop={handleDrop}
        />
        <ViewImage
          form={form}
          handleDelete={handleDelete}
          viewImgShow={viewImgShow}
          setViewImgShow={setViewImgShow}
        />

        <CustomButton label="+" sizeVariant="CUSTOM" onClick={() => setAddImgModalShow(true)} className='rounded-circle fs-5 position-fixed bottom-0 end-0 m-3 mb-3 d-center' />
      </div>
    </Container>
  )
}
export default GalleryImages