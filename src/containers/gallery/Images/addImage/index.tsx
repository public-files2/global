import { CustomButton, Text, TextField } from "@/conponents"
import { Form, Modal } from "react-bootstrap"

const AddImage = (props: any) => {
  const {
    handleAddData,
    form,
    addImgModalShow,
    closeImgModal,
    handleDrop,
  } = props
  const handleChangeImage = (event: any) => {
    handleDrop(event.target.files[0]);
  };

  const handleSelect = () => {
    document.getElementById('fileInput')?.click();
  }
  return (
    <div className="d-center" >
      <Modal
        show={addImgModalShow} onHide={() => closeImgModal()}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <Text>Add New Image</Text>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="text-center">
            <div onClick={() => handleSelect()} style={{ cursor: 'pointer' }}>
              <img src={form?.url ? form?.url : "/assets/camera.webp"} className="rounded-circle" width="170px" height="170px" />
              <div>
                <Text>Choose here</Text>
              </div>
            </div>
            <Form>
              <input
                type="file"
                id="fileInput"
                style={{ display: 'none' }}
                onChange={(e: any) => handleChangeImage(e)}
              />
            </Form>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <div className="p-2 d-center gap-1 justify-content-end" >
            <CustomButton sizeVariant="SMALL" label="Close" onClick={() => closeImgModal()} />
            <CustomButton
              sizeVariant="SMALL"
              label={"Add"} onClick={() => { handleAddData() }}
              disabled={form?.url ? false : true}
            />
          </div>
        </Modal.Footer>
      </Modal>
    </div>
  )
}
export default AddImage