
import { KEYS } from "@/api/handlers";
import { add_image, delete_image, update_image } from "@/api/services/image";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { getGalleryDetail } from "@/api/services/gallery";
import useFileUpload from "@/containers/utils/hooks/useFileUpload";
import { useRouter } from "next/router";
import { AuthContext } from "@/context/user";
import { useForm } from "@/containers/utils/hooks/useForm";

const useImagesHook = () => {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
  const { form, setForm, handleChange: handleChange } = useForm();
  const { imageUrl, callImageUpload, setImageUrl } = useFileUpload();
  const [addImgModalShow, setAddImgModalShow] = useState(false);
  const [viewImgShow, setViewImgShow] = useState(false);
  const closeImgModal = () => {
    setAddImgModalShow(false)
    setForm({ ...form, url: '' })
  }
  const router = useRouter();
  const { folderId, userId } = router.query;
  useEffect(() => {
    if (imageUrl) {
      setForm({ ...form, url: imageUrl });
    }
  }, [imageUrl]);

  const handleDrop = (acceptedFiles: any) => {
    const file = acceptedFiles;
    if (file) {
      const formData = new FormData();
      formData.append('image', file);
      callImageUpload(formData, 'gallery');
    }
  };

  const clearForm = () => {
    setForm({ ...form, url: '', title: '' })
  }
  useEffect(() => {
    setForm({ ...form, userId: loggedInUser?._id || '' })
  }, [form.userId])

  useEffect(() => {
    setForm({ ...form, folderId: folderId || "" })
  }, [form?.folderId])

  const { data, refetch } = useQuery({
    queryKey: [KEYS.gallery, folderId, userId],
    queryFn: getGalleryDetail,
    enabled: !!folderId
  });

  const addMutation = useMutation({
    mutationFn: (payload) => add_image(payload),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      refetch()
      toast.success("image added")
      closeImgModal()
      setImageUrl('')
      clearForm()
    },
  });
  const handleAddData = () => {
    let finalData: any = {
      url: form?.url,
      title: form?.title,
      folderId: form?.folderId || folderId,
      userId: loggedInUser?._id || form?.userId
    }
    if (finalData?.url) {
      if (finalData?.userId) {
        addMutation.mutate(finalData);
      } else {
        toast.warning("Something went wrong")
      }
    } else {
      toast.warning("select a image first")
    }
  }

  const deleteMutation = useMutation({
    mutationFn: (id) => delete_image(id),
    onError: (error) => {
      console.log(error, `rolling back optimistic update`);
    },
    onSuccess: (data) => {
      refetch()
      setViewImgShow(false)
      toast.error("Image deleted")
      clearForm()
    },
  });
  const handleDelete = (id: any) => {
    deleteMutation.mutate(id)
  }

  const handleEdit = (singleData: any) => {
    setForm({ ...singleData, editId: singleData?._id })
  }

  return {
    handleAddData,
    handleDelete,
    handleEdit,
    data,
    handleChange,
    form,
    loggedInUser,
    setForm,
    handleDrop,
    addImgModalShow,
    setAddImgModalShow,
    closeImgModal,
    viewImgShow,
    setViewImgShow,
  }
}
export default useImagesHook