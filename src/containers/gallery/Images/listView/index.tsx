import { Table } from 'react-bootstrap';
import { CustomButton, Text } from '@/conponents';
import { FaRegEye } from 'react-icons/fa';
import { convertDateFormat, convertDateFormatToTime } from '@/containers/utils/functions/date';
import { colors } from '@/constant';

const ListView = (props: any) => {
  const { data, handleEdit, setViewImgShow } = props
  const tableHead = [
    { id: 1, label: 'Sr.No', minWidth: '' },
    { id: 2, label: 'Image', minWidth: 100 },
    { id: 3, label: 'Last updated', minWidth: '' },
    { id: 4, label: 'Time', minWidth: '' },
    { id: 5, label: 'Action', minWidth: '' },
  ];
  return (
    <div className='mt-5 rounded'>
      <Table responsive className=''>
        <thead>
          <tr>
            {tableHead.map((column, index) => (
              <th
                key={index}
                className={`text-start`}
                style={{ minWidth: column.minWidth }}
              >
                <Text variant='h1' size={16} color={colors?.black}>{column.label}</Text>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data?.map((item: any, index: any) => {
            return (
              <tr key={index}>
                <td>
                  <Text>{index + 1}</Text>
                </td>
                <td>
                  <img src={item?.url} className='rounded-circle' width='50px' height={'50px'} />
                </td>
                <td>
                  <Text>{convertDateFormat(item?.updatedAt)}</Text>
                </td>
                <td>
                  <Text>{convertDateFormatToTime(item?.updatedAt)}</Text>
                </td>
                <td>
                  <div className=''>
                    {/* <CustomButton sizeVariant="CUSTOM" className='rounded-circle pt-1 px-3' label={<MdDelete />} onClick={() => handleDelete(form?.editId)} /> */}
                    <CustomButton
                      sizeVariant="CUSTOM"
                      className='rounded-circle pt-1 px-3'
                      label={<FaRegEye />}
                      onClick={() => { handleEdit(item); setViewImgShow(true) }}
                    />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  )
}
export default ListView