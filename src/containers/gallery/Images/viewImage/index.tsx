import { CustomButton, Text, TextField } from "@/conponents"
import { Form, Modal } from "react-bootstrap"
import { MdDelete } from "react-icons/md"

const ViewImage = (props: any) => {
  const {
    handleDelete,
    form,
    viewImgShow,
    setViewImgShow,
  } = props
  return (
    <div className="d-center" >
      <Modal
        show={viewImgShow} onHide={() => setViewImgShow(false)}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <div className="p-2 d-center justify-content-end" >
              <CustomButton sizeVariant="CUSTOM" className='rounded-circle pt-1' label={<MdDelete className='fs-6' />} onClick={() => handleDelete(form?.editId)} />
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="text-center">
            <img src={form?.url ? form?.url : ""} className="rounded-3" width="100%" />
          </div>
        </Modal.Body>
      </Modal>
    </div>
  )
}
export default ViewImage