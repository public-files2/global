import { Col, Row } from 'react-bootstrap';
import { Text } from '@/conponents';
import styles from '../styles.module.css'
import Slider from "react-slick";

const GridView = (props: any) => {
  const { data, setViewImgShow, handleEdit } = props
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1500
  };
  return (
    <div className='mt-3'>
      <div>
        <div className='px-md-4 px-1 mb-5'>
          {
            data?.filter((value: any) => value?.isPopular)?.length > 3 &&
            <>
              <Slider {...settings}>
                {data?.filter((value: any) => value?.isPopular)?.map((item: any, index: any) => {
                  return (
                    <div key={index} className='mb-3 h-100 px-2'>
                      <div className='overflow-hidden rounded'>
                        <img width="100%" className={`${styles.gridSliderImg} shadow-lg`} src={item?.url} alt="img" />
                      </div>
                    </div>
                  );
                })}
              </Slider>
            </>
          }
        </div>
        <div className='px-md-5'>
          <Row className='justify-content-center'>
            {
              data?.length > 0 ?
                <>
                  {data?.map((item: any, index: any) => {
                    return (
                      <Col sm={3} xs={4} key={index} className='mb-3 h-100' onClick={() => { handleEdit(item); setViewImgShow(true) }}>
                        <div className='scaleItems overflow-hidden rounded' style={{ cursor: "pointer" }}>
                          <img width="100%" className={`${styles.gridImg}`} height="150" src={item?.url} alt="graph--v1" />
                        </div>
                      </Col>
                    );
                  })}
                </>
                :
                <Col style={{ height: '300px' }} className='d-center flex-column gap-2'>
                  <Text>No Image found</Text>
                  <Text>Add new Image</Text>
                </Col>
            }
          </Row>
        </div>
      </div>
    </div >
  )
}
export default GridView