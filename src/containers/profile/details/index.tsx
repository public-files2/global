import { AiOutlineMail } from 'react-icons/ai';
import { BiPhoneCall, BiUser } from 'react-icons/bi';
import styles from './style.module.css';
import React from 'react';
import { IMG_BASE_URL } from '@/constant/basicConstant';
import { CustomButton, Text } from '@/conponents';

const Details = (props: any) => {
  const { user = {}, handleOpenEditProfile } = props;
  return (
    <div className={`transparentBg`}>
      <div className={`text-center p-2`}>
        <img
          src={
            user?.profileImg
              ? user?.profileImg
              : '/assets/camera.webp'
          }
          className={`${styles.profileImage} rounded-circle`}
          alt="image"
        />
      </div>
      <div className='d-flex overflow-hidden'>
        <div className='overflow-scrol'>
          {user?.firstName && (
            <div className='d-flex gap-1'>
              <BiUser />
              <Text textTransform='capitalize'>
                {user?.firstName} {user?.lastName}
              </Text>
            </div>
          )}
          {user?.mobile && (
            <div className='d-flex gap-1'>
              <BiPhoneCall />
              <Text size={12}>
                {user?.mobile}
              </Text>
            </div>
          )}
          {user?.email && (
            <div className='d-flex flex-wrap gap-1'>
              <AiOutlineMail />
              <Text size={10}>
                {user?.email}
              </Text>
            </div>
          )}
        </div>
      </div>
      <div className='py-3 text-center'>
        <CustomButton
          onClick={handleOpenEditProfile}
          label={<Text lineHeight={0} size={10} color={"white"}>Edit Profile</Text>}
          sizeVariant="CUSTOM"
          className="py-1 d-center"
        />
      </div>
    </div>
  );
};

export default Details;
