import { FiUpload } from 'react-icons/fi';
import { RxCross2 } from 'react-icons/rx';
import { MdDelete } from 'react-icons/md';
import { Modal, ModalBody, ModalTitle } from 'react-bootstrap';
import { CustomButton, Text, TextField } from '@/conponents';
const EditProfileModal = (props: any) => {
   const {
      openEditProfile,
      handleCloseEditProfile,
      profileData,
      setProfileData,
      handleSubmit,
      handleDrop,
      handleRemove,
   } = props;

   const handleChange = (event: any) => {
      handleDrop(event.target.files[0]);
   };
   return (
      <Modal show={openEditProfile} onHide={() => handleCloseEditProfile()}>
         <ModalTitle>
            <div
               className="d-center justify-content-between px-2"
            >
               <Text>
                  Edit Profile
               </Text>
               <div>
                  <CustomButton sizeVariant="CUSTOM" className='d-center' label={<RxCross2 />} onClick={handleCloseEditProfile} />
               </div>
            </div>
         </ModalTitle>
         <ModalBody>
            <div className='text-center'>
               <img
                  className="rounded-circle"
                  src={
                     profileData?.profileImg
                        ? profileData?.profileImg
                        : '/assets/camera.webp'
                  }
                  width="100px"
                  height="100px"
                  alt="image"
               />
               <div>
                  <label role="button" htmlFor="file" className="d-center ">
                     <Text
                        fontSize="15px"
                        fontWeight={600}
                     >
                        Change Image
                     </Text>
                     <FiUpload size={15} className='mt-1 ms-1' />
                  </label>
                  <input
                     id="file"
                     type="file"
                     className="d-none"
                     accept="image/*"
                     onChange={handleChange}
                  />
                  <label role="button" onClick={handleRemove} className="d-center">
                     <Text
                        fontSize="15px"
                        fontWeight={600}
                     >
                        Remove Photo
                     </Text>
                     <MdDelete size={15} className='mt-1' />
                  </label>
               </div>
            </div>
            <div>
               <TextField
                  label="First Name"
                  value={profileData?.firstName}
                  onChange={(e: any) =>
                     setProfileData({
                        ...profileData,
                        firstName: e.target.value,
                     })
                  }
               />
               <TextField
                  label="Last Name"
                  value={profileData?.lastName}
                  onChange={(e: any) =>
                     setProfileData({
                        ...profileData,
                        lastName: e.target.value,
                     })
                  }
               />
            </div>
            <div >
               <TextField
                  label="Email"
                  value={profileData?.email}
                  onChange={(e: any) =>
                     setProfileData({
                        ...profileData,
                        email: e.target.value,
                     })
                  }
                  placeholder="example@gamil.com"
               />
            </div>
            <div>
               <TextField
                  label="Phone no."
                  value={profileData?.mobile}
                  onChange={(e: any) =>
                     setProfileData({
                        ...profileData,
                        mobile: e.target.value,
                     })
                  }
                  placeholder="xxxxx00000"
               />
            </div>
            <div className='d-center justify-content-end gap-2 mt-2'>
               <CustomButton
                  sizeVariant="SMALL"
                  label="Cancel"
                  onClick={handleCloseEditProfile}
               />
               <CustomButton
                  label="Save"
                  sizeVariant="SMALL"
                  onClick={() => handleSubmit()}
               />
            </div>
         </ModalBody>
      </Modal>
   );
};

export default EditProfileModal;
