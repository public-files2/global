import * as React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { AuthContext } from '../../context/user';
import Details from './details';
import EditProfileModal from './details/edit-profile-modal';
import { Text } from '@/conponents';
import useProfile from './hooks';

const Profile = () => {
  const {
    profileData,
    setProfileData,
    handleSubmit,
    handleDrop,
    handleRemove,
    openEditProfile,
    handleCloseEditProfile,
    handleOpenEditProfile,
    loggedInUser,
    setLoggedInUser,
  } = useProfile();



  return (
    <div>
      <Container>
        <div className='text-center'>
          <Text
            variant="h3"
            fontWeight={800}
          >
            My Profile
          </Text>
          <Text
            variant="body2"
            fontWeight={700}
          >
            Welcome Back!👋
          </Text>
        </div>
        <div>
          <Row className='justify-content-center'>
            <Col md={3}>
              <div className="mb-4">
                <Details
                  user={loggedInUser}
                  handleOpenEditProfile={
                    handleOpenEditProfile
                  }
                />
              </div>
            </Col>
          </Row>
        </div>
      </Container>
      <EditProfileModal
        openEditProfile={openEditProfile}
        handleCloseEditProfile={handleCloseEditProfile}
        profileData={profileData}
        handleSubmit={handleSubmit}
        setProfileData={setProfileData}
        setLoggedInUser={setLoggedInUser}
        handleDrop={handleDrop}
        handleRemove={handleRemove}
      />
    </div>
  );
};

export default Profile;
