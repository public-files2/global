import { Col, Container, Row } from "react-bootstrap"
import { Gallery2 } from "../gallery"
import { Text } from "@/conponents"
import { GrGallery } from "react-icons/gr";
import { colors } from "@/constant";
import { projectsList } from "@/constant/data";
import Link from "next/link";

const HomePage = () => {

  return (
    <Container className="transparentBg py-5">
      <div className="text-center">
        <Text variant='h1'>
          Tasks List
        </Text>
      </div>
      <Row className="justify-content-center">
        {
          projectsList?.map((item: any, index: any) => {
            return (
              <Col md={3} xs={4} key={index}>
                <div className={`mt-5 rounded  transparentBg`}>
                  <Link href={item?.link} passHref>
                    <div className='d-center w-100 flex-column py-2'>
                      <Text size={60} color={colors?.secondryColor} className="mb-2">
                        {item?.icon}
                      </Text>
                      <Text size={10} color={colors?.primaryColor}>
                        {item?.projectName}
                      </Text>
                    </div>
                  </Link>
                </div>
              </Col>
            )
          })
        }
      </Row>
    </Container>
  )
}
export default HomePage