import { CustomButton, TextField } from "@/conponents";
import React, { useState } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import DropdownField from "@/conponents/dropdown";

import { inputTypesList } from "@/constant/data";

const AddFormFields = (props: any) => {
  const {
    handleChange,
    form,
    handleSubmit,
  } = props

  return (
    <>
      {
        form?.formName &&
        <Row>
          <Col md={12}>
            <DropdownField
              label="Select input type"
              options={inputTypesList}
              name="inputType"
              onChange={handleChange}
              value={form?.inputType && form?.inputType}
            />
          </Col>
          <Col md={6}>
            <TextField
              label="Form Label"
              name="label"
              value={form?.label || ""}
              onChange={handleChange}
            />
          </Col>
          <Col md={6}>
            <TextField
              label="Value name"
              name="valueName"
              value={form?.valueName || ""}
              onChange={handleChange}
            />
          </Col>
          <Col md={6}>
            <TextField
              label="Grid Number"
              name="gridNumber"
              type="number"
              value={form?.gridNumber || ""}
              onChange={handleChange}
            />
          </Col>

          <Col md={12}>
            <CustomButton className='mt-4' label="Add field" onClick={(e: any) => handleSubmit(e)} />
          </Col>
        </Row>
      }
    </>
  );
};

export default AddFormFields;
