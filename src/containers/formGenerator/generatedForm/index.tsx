import { CustomButton, Text, TextField } from "@/conponents";
import React, { useState } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { convertCamelCase } from "@/containers/utils/functions";
import { MdDelete } from "react-icons/md";

const GeneratorForm = (props: any) => {
  const {
    handleChange,
    form,
    formConfig,
    handleAddData,
    deleteField,
  } = props
  return (
    <>
      {
        form?.formName &&
        <>
          <div className="text-center mb-3">
            <h2>{form?.formName}</h2>
          </div>
          <Row>
            {formConfig.map((item: any, index: any) => (
              <Col md={item?.gridNumber ? item?.gridNumber : 12} key={index} className="position-relative">
                <TextField
                  label={item?.label}
                  name={item?.valueName ? convertCamelCase(item?.valueName) : convertCamelCase(item?.label)}
                  value={form?.[item?.name]}
                  type={item?.inputType ? item?.inputType : 'text'}
                  onChange={handleChange}
                  className="w-100"
                />
                <Text className="position-absolute" size={15} style={{ top: "10px", right: "10px", cursor: "pointer" }}>
                  <MdDelete onClick={() => deleteField(index)} />
                </Text>
              </Col>
            ))}
          </Row>
          {
            formConfig?.length > 0 &&
            <div className="text-center mt-3">
              <CustomButton
                sizeVariant="SMALL"
                label="Save"
                onClick={() => { handleAddData() }}
              />
            </div>
          }
        </>
      }
    </>
  );
};

export default GeneratorForm;
