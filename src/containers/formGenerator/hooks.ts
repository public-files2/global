
import { AuthContext } from "@/context/user";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { addForm, deleteForm, getForm, updateForm } from "@/api/services/formGen";
import { useForm } from "@/containers/utils/hooks/useForm";
import { useRouter } from "next/router";

const useFormHook = () => {
  const { loggedInUser, setLoggedInUser } = useContext(AuthContext);
  const { form, setForm, handleChange } = useForm();
  const [formConfig, setFormConfig] = useState<any>([])
  const router = useRouter()
  useEffect(() => {
    setForm({ ...form, userId: loggedInUser?._id })
  }, [form?.userId])

  // const { data, refetch } = useQuery({
  //   queryKey: ["FORMS", loggedInUser?._id],
  //   queryFn: getForm,
  //   enabled: !!loggedInUser?._id,
  // });

  const addMutation = useMutation({
    mutationFn: (payload) => addForm(payload),
    onError: (error) => {
      console.log(error, `on create`);
    },
    onSuccess: () => {
      router.push('generatedForm')
      clearForm()
      setFormConfig([])
      toast.success("form added")
    },
  });

  const handleAddData = () => {
    let finalData: any = {
      formName: form?.formName,
      formDetails: formConfig,
      userId: loggedInUser?._id || form?.userId,
    }
    if (finalData?.formName) {
      addMutation.mutate(finalData);
    } else {
      toast.warning("please enter form Name")
    }
  }

  const handleSubmit = () => {
    if (form?.label) {
      setFormConfig([...formConfig, form])
      clearForm()
    } else {
      toast.warning("form label required!!")
    }
  };

  const clearForm = () => {
    setForm({ ...form, label: '', inputType: '', valueTitle: '', gridNumber: '', valueName: '' })
  }

  const deleteField = (index: any) => {
    formConfig.splice(index, 1)
    setFormConfig([...formConfig])
  }

  return {
    handleAddData,
    handleChange,
    deleteField,
    loggedInUser,
    form,
    setForm,
    handleSubmit,
    formConfig,
  }
}
export default useFormHook