import { CustomButton, TextField } from "@/conponents";
import React, { useState } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import useFormHook from "./hooks";
import AddFormFields from "./addFilelds";
import GeneratorForm from "./generatedForm";
import Link from "next/link";

const FormGenerator = () => {

  const {
    handleAddData,
    handleChange,
    form,
    handleSubmit,
    formConfig,
    deleteField,
  } = useFormHook()

  return (
    <Container className="transparentBg p-1">
      <Link href='/generatedForm' passHref>
        <CustomButton label="Forms List" />
      </Link>
      <Row className="justify-content-center text-center">
        <Col md={6}>
          <TextField
            label="Form Name"
            name="formName"
            value={form?.formName || ""}
            onChange={handleChange}
          />
        </Col>
      </Row>
      <Row>
        <Col md={6}>
          <AddFormFields
            handleChange={handleChange}
            form={form}
            handleSubmit={handleSubmit}
          />
        </Col>
        <Col md={6}>
          <GeneratorForm
            handleChange={handleChange}
            form={form}
            formConfig={formConfig}
            handleAddData={handleAddData}
            deleteField={deleteField}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default FormGenerator;
