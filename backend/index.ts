//Create HTTP server and listen on port 3000 for requests
import bodyParser from 'body-parser';
import cors from 'cors';
import express, { Application } from 'express';
import connectDB from './src/config/db';
import { errorHandler } from './src/middlewares/errorHandler';
import { notFoundHandler } from './src/middlewares/notFoundHandler';
import routes from './src/routes/v1/index';
import 'dotenv/config'
connectDB();
const app: Application = express();
const port = process.env.PORT || 5001;

// Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Cors Middleware
app.use(
   cors({
      origin: '*',
   })
);

// Routes
app.use('/', routes);

// Error handling middleware
app.use(errorHandler);
app.use(notFoundHandler);

// Start server
app.listen(port as number, '0.0.0.0', () => console.log(`Server is started on port ${port}`));

