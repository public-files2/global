import { PostModel } from '../models/post.model';

class PostService {
   static async getAllPost(
      fileType: any,
      userId?: any,
   ) {
      const query: any = {};
      if (userId) {
         query.userId = userId;
      }
      if (fileType) {
         query.fileType = fileType;
      }
      const count = await PostModel.countDocuments(query);

      const postImages = await PostModel.find(query).sort({ createdAt: 'desc' }).populate({ path: "userId", select: 'userName firstName' }).populate({ path: 'comments.userId', select: 'userName' }).exec();
      return {
         count: count,
         data: postImages,
      };
   }

   static async getPostById(id: string) {
      return await PostModel.findById(id).populate({ path: "userId", select: 'userName' }).populate({ path: 'comments.userId', model: 'User', select: 'userName' }).exec();
   }

   static async createPost(PostData: any) {
      const newPost = new PostModel(PostData);
      return await newPost.save();
   }

   static async updatePost(id: string, PostData: any) {
      return await PostModel.findByIdAndUpdate(id, PostData, {
         new: true,
         runValidators: true,
      }).exec();
   }

   static async deletePost(id: string) {
      await PostModel.findByIdAndDelete(id).exec();
   }

   static async likePost(postId: string, userId: string) {
      return await PostModel.findByIdAndUpdate(
         postId,
         { $addToSet: { likes: userId } },
         { new: true }
      ).exec();
   }

   static async unlikePost(postId: string, userId: string) {
      return await PostModel.findByIdAndUpdate(
         postId,
         { $pull: { likes: userId } },
         { new: true }
      ).exec();
   }

   static async addComment(postId: string, comment: any) {

      return await PostModel.findByIdAndUpdate(
         postId,
         { $push: { comments: comment } },
         { new: true }
      ).exec();
   }

   static async deleteComment(postId: string, commentId: string) {
      return await PostModel.findByIdAndUpdate(
         postId,
         { $pull: { comments: { _id: commentId } } },
         { new: true }
      ).exec();
   }
}

export default PostService;
