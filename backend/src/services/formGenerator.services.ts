import { FormModel } from '../models/formGenerator.model';

class FormService {
   static async getAllForm(userId: any, formName: any,) {
      const query: any = {};

      if (userId !== undefined) {
         query.userId = userId;
      }
      if (formName !== undefined) {
         query.formName = formName;
      }

      const count = await FormModel.countDocuments(query);
      const forms = await FormModel.find(query).exec();

      return {
         count: count,
         data: forms,
      };
   }

   static async getFormById(id: string) {
      return await FormModel.findById(id).exec();
   }

   static async createForm(FormData: any) {
      const newForm = new FormModel(FormData);
      return await newForm.save();
   }

   static async updateForm(id: string, FormData: any) {
      return await FormModel.findByIdAndUpdate(id, FormData, {
         new: true,
         runValidators: true,
      }).exec();
   }

   static async deleteForm(id: string) {
      await FormModel.findByIdAndDelete(id).exec();
   }

}

export default FormService;
