import { GalleryModel } from '../models/gallery.model';
import { FolderModel } from '../models/folders.model';

class FolderService {
   static async getAllFolder(userId: any, folderName: any,) {
      const query: any = {};

      if (userId !== undefined) {
         query.userId = userId;
      }
      if (folderName !== undefined) {
         query.folderName = folderName;
      }

      const count = await FolderModel.countDocuments(query);
      const folders = await FolderModel.find(query).exec();

      return {
         count: count,
         data: folders,
      };
   }

   static async getFolderById(id: string) {
      return await FolderModel.findById(id).exec();
   }
   static async createFolder(FolderData: any) {
      const newFolder = new FolderModel(FolderData);
      return await newFolder.save();
   }

   static async updateFolder(id: string, FolderData: any) {
      return await FolderModel.findByIdAndUpdate(id, FolderData, {
         new: true,
         runValidators: true,
      }).exec();
   }

   static async deleteFolder(id: string) {
      await GalleryModel.deleteMany({ folderId: id })
      await FolderModel.findByIdAndDelete(id).exec();
   }

}

export default FolderService;
