import { GalleryModel } from '../models/gallery.model';

class GalleryService {
   static async getAllGallery(
      type: any,
      fileType: any,
      folderId: any,
      userId?: any,
   ) {
      const query: any = {};
      if (userId) {
         query.userId = userId;
      }
      if (type) {
         query.type = type;
      }
      if (folderId) {
         query.folderId = folderId;
      }
      if (fileType) {
         query.fileType = fileType;
      }
      const count = await GalleryModel.countDocuments(query);

      const galleryImages = await GalleryModel.find(query).exec();
      return {
         count: count,
         data: galleryImages,
      };
   }

   static async getGalleryById(id: string) {
      return await GalleryModel.findById(id).exec();
   }
   static async createGallery(GalleryData: any) {
      const newGallery = new GalleryModel(GalleryData);
      return await newGallery.save();
   }

   static async updateGallery(id: string, GalleryData: any) {
      return await GalleryModel.findByIdAndUpdate(id, GalleryData, {
         new: true,
         runValidators: true,
      }).exec();
   }

   static async deleteGallery(id: string) {
      await GalleryModel.findByIdAndDelete(id).exec();
   }
   static async updatePriorities(title: string, newPriority: number) {
      await GalleryModel.updateOne({ title: title }, { $set: { priority: newPriority } });

      const objectsToUpdate = await GalleryModel.find({ priority: { $gte: newPriority }, name: { $ne: title } });
      const updatedObjects = await Promise.all(
         objectsToUpdate.map(async (obj) => {
            obj.priority++;
            return obj.save();
         })
      );
   }
}

export default GalleryService;
