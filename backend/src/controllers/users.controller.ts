import bcrypt from 'bcrypt';
import { randomBytes } from 'crypto';
import { NextFunction, Request, Response } from 'express';
import {
   createUser,
   deleteUser,
   getAllUsers,
   getUserById,
   getUserByIdV2,
   getUserByUsername,
   updateUser,
} from '../services/user.services';
import { loginOtp } from '../smsTemplates/loginOtp';
import { sendMailSignOtp } from '../config/mailer';

export const registerUser = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const {
         firstName = '',
         lastName = '',
         email = '',
         mobile = '',
         profileImg = '',
         userName = '',
         password = '',
         viewType = "",
         folders = [],
      } = req?.body;
      let existingUser

      if (!userName) {
         return res.status(400).json({ message: 'Username is required.', status: 'success' });
      } else {
         existingUser = await getUserByUsername(userName);
      }
      if (existingUser) {
         return res.status(400).json({ message: 'Username already exists' });
      }
      const saltRounds = 10;
      const hashedPassword = password ? await bcrypt.hash(password, saltRounds) : '';
      const data = {
         firstName: firstName,
         lastName: lastName,
         email: email,
         mobile: mobile,
         profileImg: profileImg,
         userName: userName,
         password: hashedPassword,
         viewType: viewType,
         folders: folders,
      };
      const savedUser = await createUser(data);
      res.status(201).json({ msg: 'User Created', savedUser });

   } catch (error) {
      next(error);
   }
};

export const getAll = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const { type, city } = req.query;
      const users = await getAllUsers(type, city);
      res.json(users);
   } catch (error) {
      next(error);
   }
};

export const getById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const users: any = await getUserById(req.params.id);
      if (!users) {
         return res.status(404).json({ message: 'Users not found' });
      }
      res.json({ ...users?._doc });
   } catch (error) {
      next(error);
   }
};

export const updateById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const {
         firstName,
         lastName,
         email,
         mobile,
         profileImg,
         userName,
         password,
         viewType,
         folders,
      } = req?.body;

      const user = await getUserByIdV2(req.params.id);
      if (!user) {
         return res.status(404).json({ message: 'User not found' });
      }

      const saltRounds = 10;
      let hashedPassword: string | null = null;
      if (password) {
         hashedPassword = await bcrypt.hash(password, saltRounds);
      }
      const data = {
         firstName: firstName || user.firstName,
         lastName: lastName || user.lastName,
         email: email || user.email,
         mobile: mobile || user.mobile,
         profileImg: profileImg || user.profileImg,
         userName: userName || user.userName,
         password: hashedPassword || user.password,
         viewType: viewType || user.viewType,
         folders: folders || user?.folders
      };
      const updatedUser: any = await updateUser(req.params.id, data);
      res.json(updatedUser);
   } catch (error) {
      next(error);
   }
};

export const deleteById = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const users = await deleteUser(req.params.id);
      if (!users) {
         return res.status(404).json({ message: 'Users not found' });
      }
      res.json({ message: 'Users deleted' });
   } catch (error) {
      next(error);
   }
};

export const loginUser = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const userName = req.body.userName;
      const inputPassword = req.body.password;
      let user;
      if (!userName) {
         return res.status(400).json({ message: 'Username is required.', status: 'success' });
      } else {
         user = await getUserByUsername(userName);
      }
      if (user) {
         // const token = generateToken({ userId: user._id.toString() });
         const hashedPassword = user.password;
         const isMatch = await bcrypt.compare(inputPassword, hashedPassword);
         if (isMatch) {
            return res.status(200).json(user);
         } else {
            return res.status(400).json({ message: 'Invalid password.', status: 'success' });
         }
      } else {
         return res.status(400).json({ message: 'User does not exist.', status: 'success' });
      }
   } catch (error) {
      next(error);
   }
};

export async function sendOTP(req: Request, res: Response, next: NextFunction): Promise<any> {
   const { email, userName } = req?.body;
   try {
      const { otp, otpMessage } = loginOtp();
      if (!email) {
         return res.status(404).json({ message: 'email not found' });
      }
      const mailData = {
         userName: userName,
         otp: otp,
      };
      const subject = 'Registration Confirmation OTP';
      const isMailed: any = await sendMailSignOtp(email, subject, mailData);
      if (isMailed != "Email sent") {
         return res.status(400).json({ message: 'Failed to send Email', err: isMailed });
      }
      return res.status(200).json({ message: 'Email sent', otp: otp });
   } catch (err) {
      next(err);
   }
}

export async function verifyOTP(req: Request, res: Response, next: NextFunction): Promise<any> {
   const userName = req?.body?.userName;
   try {
      const { otp, otpMessage } = loginOtp();
      const user = await getUserByUsername(userName);
      if (user) {
         return res.status(201).json({ message: `OTP sent Successfully ${otp}`, otp: otpMessage });
      } else {
         return res.status(401).json({ message: 'User does not exist' });
      }
   } catch (err) {
      next(err);
   }
}