import { Request, Response, NextFunction } from 'express';
import FormService from '../services/formGenerator.services';

class FormController {
   static async createForm(req: Request, res: Response, next: NextFunction) {
      try {
         const { formName, userId, formDetails } = req.body;
         const formData = {
            formName: formName,
            formDetails: formDetails,
            userId: userId,
         };
         const newForm = await FormService.createForm(formData);
         return res.status(201).json(newForm);
      } catch (error) {
         next(error);
      }
   }

   static async getAllForm(req: Request, res: Response, next: NextFunction) {
      try {
         const { userId, formName } = req.query;
         const form = await FormService.getAllForm(
            userId,
            formName
         );
         return res.status(200).json(form);
      } catch (error) {
         next(error);
      }
   }

   static async getFormById(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const form = await FormService.getFormById(id);
         return res.status(200).json(form);
      } catch (error) {
         next(error);
      }
   }

   static async updateForm(req: Request, res: Response, next: NextFunction) {
      try {
         const { formName, userId, formDetails } = req.body;
         const formData = {
            formName: formName,
            formDetails: formDetails,
            userId: userId,
         };

         const updatedForm = await FormService.updateForm(req.params.id, formData);
         return res.status(200).json(updatedForm);
      } catch (error) {
         next(error);
      }
   }

   static async deleteForm(req: Request, res: Response, next: NextFunction) {
      try {
         await FormService.deleteForm(req.params.id);
         return res.sendStatus(204);
      } catch (error) {
         next(error);
      }
   }
}

export default FormController;
