import { Request, Response, NextFunction } from 'express';
import FolderService from '../services/folders.services';

class FolderController {
   static async getAllFolder(req: Request, res: Response, next: NextFunction) {
      try {
         const { userId, folderName } = req.query;
         const folder = await FolderService.getAllFolder(
            userId,
            folderName
         );
         return res.status(200).json(folder);
      } catch (error) {
         next(error);
      }
   }

   static async getFolderById(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const folder = await FolderService.getFolderById(id);
         return res.status(200).json(folder);
      } catch (error) {
         next(error);
      }
   }

   static async createFolder(req: Request, res: Response, next: NextFunction) {
      try {
         const { folderName, type, fileType, isPopular, title, url, userId } = req.body;
         const folderData = {
            folderName: folderName,
            userId: userId,
         };
         const newFolder = await FolderService.createFolder(folderData);
         return res.status(201).json(newFolder);
      } catch (error) {
         next(error);
      }
   }

   static async updateFolder(req: Request, res: Response, next: NextFunction) {
      try {
         const { folderName } = req.body;
         const folderData = {
            folderName: folderName,
         };

         const updatedFolder = await FolderService.updateFolder(req.params.id, folderData);
         return res.status(200).json(updatedFolder);
      } catch (error) {
         next(error);
      }
   }

   static async deleteFolder(req: Request, res: Response, next: NextFunction) {
      try {
         await FolderService.deleteFolder(req.params.id);
         return res.sendStatus(204);
      } catch (error) {
         next(error);
      }
   }
}

export default FolderController;
