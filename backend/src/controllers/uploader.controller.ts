import { NextFunction, Request, Response } from 'express';
import { UploadedFile } from 'express-fileupload';
import fs from 'fs';
import path from 'path';
import sharp from 'sharp';
import cloudinaryUpload from '../config/cloudinaryConfig';
import uploadToFirebase from '../config/firebaseConfig';

export const uploadImage = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const file = req.file as unknown as UploadedFile & { buffer: Buffer };
      if (!file) throw new Error('No file uploaded');
      const { buffer, mimetype } = file;
      let isImage = mimetype.startsWith('image/');

      if (!isImage) throw new Error('Invalid file type');

      const imgType = req.query.imgType;
      let directoryPath = '';

      if (imgType === 'users') {
         directoryPath = path.join(__dirname, '..', '../public/users');
      } else if (imgType === 'gallery') {
         directoryPath = path.join(__dirname, '..', '../public/gallery');
      } else if (imgType === 'img') {
         directoryPath = path.join(__dirname, '..', '../public/img');
      }

      const extension = `.${mimetype.split('/')[1]}`;
      const filename = `${Date.now()}${extension}`;
      const filepath = path.join(directoryPath, filename);
      if (!fs.existsSync(directoryPath)) {
         fs.mkdirSync(directoryPath, { recursive: true });
      }
      await sharp(buffer).resize({ width: 500, withoutEnlargement: true }).toFile(filepath);
      const result = await uploadToFirebase(filepath, filename);
      // const result = await cloudinaryUpload(filepath);
      console.log(result?.secure_url[0])
      const data = {
         message: 'Image uploaded successfully',
         imgUrl: result?.secure_url[0], //for firebase
         // imgUrl: result?.secure_url, //for cloudinary
      }
      return res.status(201).json(data);
   } catch (err) {
      console.log(err, 'error')
      next(err);
   }
};