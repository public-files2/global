import { Request, Response, NextFunction } from 'express';
import fs from 'fs-extra';
import path from 'path';
import archiver from 'archiver';

export const getAllImages = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const rootDir = path.join(__dirname, '..', '../public');
      const imageExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp'];
      const images = await getAllImagesInDirectory(rootDir, imageExtensions);
      return res.status(200).json(images);
   } catch (err) {
      next(err);
   }
};

async function getAllImagesInDirectory(directoryPath: string, imageExtensions: string[]): Promise<string[]> {
   try {
      const files = await fs.promises.readdir(directoryPath);
      const imageFiles = files
         .filter((file) => {
            const extension = path.extname(file).toLowerCase();
            return imageExtensions.includes(extension);
         })
         .map((file) => path.join('/', path.relative(path.join(__dirname, '..', '../public'), directoryPath), file));
      const subdirectories = files
         .filter((file) => {
            const stats = fs.statSync(path.join(directoryPath, file));
            return stats.isDirectory();
         })
         .map((dir) => path.join(directoryPath, dir));
      const subdirectoryImages = await Promise.all(
         subdirectories.map((dir) => getAllImagesInDirectory(dir, imageExtensions))
      );
      return subdirectoryImages.flat().concat(imageFiles);
   } catch (err) {
      throw err;
   }
}

export const getImageByType = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const imgType = req.query.imgType;
      let directoryPath = '';
      if (imgType === 'users') {
         directoryPath = path.join(__dirname, '..', '../public/users');
      } else if (imgType === 'services') {
         directoryPath = path.join(__dirname, '..', '../public/services');
      } else if (imgType === 'products') {
         directoryPath = path.join(__dirname, '..', '../public/products');
      } else if (imgType === 'banner') {
         directoryPath = path.join(__dirname, '..', '../public/gallery');
      } else if (imgType === 'gallery') {
         directoryPath = path.join(__dirname, '..', '../public/gallery');
      } else {
         return res.status(400).json({ message: 'Invalid imgType parameter' });
      }
      const files = await fs.promises.readdir(directoryPath);
      const images = files
         .filter((file) => {
            const extension = path.extname(file).toLowerCase();
            return ['.jpg', '.jpeg', '.png', '.gif', '.bmp'].includes(extension);
         })
         .map((file) => `/${imgType}/${file}`);
      return res.status(200).json(images);
   } catch (err) {
      next(err);
   }
};

export const deleteImages = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const rootDir = path.join(__dirname, '..', '../public');
      const imageExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp'];
      const images = await getAllImagesInDirectory(rootDir, imageExtensions);
      return res.status(200).json({ message: 'image Deleted' });
   } catch (err) {
      next(err);
   }
};

export const getAllFiles = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const rootDir = path.join(__dirname, '..', '../public');
      const imageExtensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp'];
      const videoExtensions = ['.mp4', '.avi', '.mov', '.mkv'];
      const files = await getAllFilesInDirectory(rootDir, imageExtensions, videoExtensions);
      return res.status(200).json(files);
   } catch (err) {
      next(err);
   }
};

interface FileData {
   filePath: string;
   type: string;
   size: string;
}

interface FolderData {
   folderName: string;
   folderSize: string;
   files: FileData[];
}

async function getAllFilesInDirectory(
   directoryPath: string,
   imageExtensions: string[],
   videoExtensions: string[]
): Promise<FolderData[]> {
   try {
      const files = await fs.promises.readdir(directoryPath);
      const allFiles: FileData[] = [];
      const subdirectories: string[] = [];

      let folderSize = 0;

      for (const file of files) {
         const filePath = path.join(directoryPath, file);
         const stats = await fs.promises.stat(filePath);

         if (stats.isDirectory()) {
            subdirectories.push(filePath);
         } else {
            const extension = path.extname(file).toLowerCase();
            const fileSize = stats.size;

            const fileData: FileData = {
               filePath: path.join('/', path.relative(path.join(__dirname, '..', '../public'), directoryPath), file),
               type: extension.startsWith('.') ? extension.substr(1) : extension,
               size: getFileSizeUnit(fileSize),
            };

            folderSize += fileSize;

            allFiles.push(fileData);
         }
      }

      const subdirectoryFiles = await Promise.all(
         subdirectories.map((dir) => getAllFilesInDirectory(dir, imageExtensions, videoExtensions))
      );

      const folderName = path.basename(directoryPath);
      const folderData: FolderData[] = subdirectoryFiles.flat();
      folderData.push({
         folderName,
         folderSize: getFileSizeUnit(folderSize),
         files: allFiles,
      });

      return folderData;
   } catch (err) {
      throw err;
   }
}

function getFileSizeUnit(fileSize: number): string {
   const units = ['B', 'KB', 'MB', 'GB', 'TB'];
   let size = fileSize;
   let unitIndex = 0;

   while (size >= 1024 && unitIndex < units.length - 1) {
      size /= 1024;
      unitIndex++;
   }

   return `${size.toFixed(2)} ${units[unitIndex]}`;
}

export const deleteFile = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const filePath = req.query.filePath as string;
      const fullPath = path.join(__dirname, '../../public/', filePath);
      const fileExists = await fs.pathExists(fullPath);
      if (!fileExists) {
         return res.status(404).json({ message: 'File not found' });
      }
      await fs.promises.unlink(fullPath);
      return res.status(200).json({ message: 'File deleted successfully' });
   } catch (err) {
      next(err);
   }
};

export const deleteFolder = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const folderPath = req.query.folderPath as string;
      const fullPath = path.join(__dirname, '../../public/', folderPath);
      const folderExists = await fs.pathExists(fullPath);
      if (!folderExists) {
         return res.status(404).json({ message: 'Folder not found' });
      }
      await fs.remove(fullPath);
      return res.status(200).json({ message: 'Folder deleted successfully' });
   } catch (err) {
      next(err);
   }
};
export const downloadAllData = async (req: Request, res: Response, next: NextFunction) => {
   try {
      const rootDir = path.join(__dirname, '../../public');
      const zipFilePath = path.join(__dirname, '../../public/data.zip');
      const output = fs.createWriteStream(zipFilePath);
      const archive = archiver('zip', {
         zlib: { level: 9 },
      });
      archive.pipe(output);
      await addFolderToArchive(rootDir, '', archive);
      await archive.finalize();
      output.end();
      res.set('Content-Type', 'application/zip');
      res.set('Content-Disposition', 'attachment; filename=public.zip');
      const zipFileStream = fs.createReadStream(zipFilePath);
      zipFileStream.pipe(res);
   } catch (err) {
      next(err);
   }
};

async function addFolderToArchive(folderPath: string, relativePath: string, archive: archiver.Archiver): Promise<void> {
   const files = await fs.promises.readdir(folderPath);

   for (const file of files) {
      const filePath = path.join(folderPath, file);
      const stats = await fs.promises.stat(filePath);

      if (stats.isDirectory()) {
         const subfolderPath = path.join(relativePath, file);
         await addFolderToArchive(filePath, subfolderPath, archive);
      } else {
         const fileData = await fs.promises.readFile(filePath);
         const zipFilePath = path.join(relativePath, file);
         archive.append(fileData, { name: zipFilePath });
      }
   }
}
