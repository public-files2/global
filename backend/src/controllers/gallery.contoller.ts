import { Request, Response, NextFunction } from 'express';
import GalleryService from '../services/gallery.services';

class GalleryController {
   static async getAllGallery(req: Request, res: Response, next: NextFunction) {
      try {
         const { type, fileType, folderId, userId } = req.query;
         const gallery = await GalleryService.getAllGallery(
            type,
            fileType,
            folderId,
            userId
         );
         return res.status(200).json(gallery);
      } catch (error) {
         next(error);
      }
   }

   static async getGalleryById(req: Request, res: Response, next: NextFunction) {
      try {
         const { id } = req.params;
         const gallery = await GalleryService.getGalleryById(id);
         return res.status(200).json(gallery);
      } catch (error) {
         next(error);
      }
   }

   static async createGallery(req: Request, res: Response, next: NextFunction) {
      try {
         const { folderId, type, fileType, isPopular, title, url, userId } = req.body;
         const galleryData = {
            type: type,
            fileType: fileType,
            title: title,
            folderId: folderId,
            url: url,
            isPopular: isPopular,
            userId: userId,
         };
         const newGallery = await GalleryService.createGallery(galleryData);
         return res.status(201).json(newGallery);
      } catch (error) {
         next(error);
      }
   }

   static async updateGallery(req: Request, res: Response, next: NextFunction) {
      try {
         const { category, title, url, active, isPopular } = req.body;
         const galleryData = {
            title: title,
            url: url,
            active: active,
            isPopular: isPopular,
         };
         const updatedGallery = await GalleryService.updateGallery(req.params.id, galleryData);
         return res.status(200).json(updatedGallery);
      } catch (error) {
         next(error);
      }
   }

   static async deleteGallery(req: Request, res: Response, next: NextFunction) {
      try {
         await GalleryService.deleteGallery(req.params.id);
         return res.sendStatus(204);
      } catch (error) {
         next(error);
      }
   }

   static async updatePrioritiesController(req: Request, res: Response, next: NextFunction) {
      try {
         const id = req.params.bannerId;
         const newPriority = req.body.Priority;
         const gallery = await GalleryService.getGalleryById(id);
         if (gallery) {
            await GalleryService.updatePriorities(gallery.title, newPriority);
            return res.sendStatus(200);
         } else {
            return res.sendStatus(404);
         }
      } catch (error) {
         next(error);
      }
   }
}

export default GalleryController;
