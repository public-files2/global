import FormController from '../../controllers/formGenerator.contoller';
import { Router } from 'express';

const router = Router();

// GET / - Retrieve all Form
router.get('/', FormController.getAllForm);

// GET /:id - Retrieve Form By id
router.get('/:id', FormController.getFormById);

// POST / - Create a new Form
router.post('/', FormController.createForm);

// PATCH /id - Update an existing Form
router.patch('/:id', FormController.updateForm);

// PATCH /id - Update an existing Form image priority
router.patch('/priority/:id', FormController.updateForm);

// DELETE /id - Delete an existing Form
router.delete('/:id', FormController.deleteForm);

export { router as formRoute };
