import { Router } from 'express';
import {
   getImageByType,
   getAllImages,
   getAllFiles,
   deleteFile,
   deleteFolder,
   downloadAllData,
} from '../../controllers/images.controller';

const router = Router();

// GET /Images By Type
router.get('/', getAllImages);

// GET /get all files
router.get('/files', getAllFiles);

//download folder
router.get('/download', downloadAllData);

// GET /Images By Type
router.get('/type', getImageByType);

// DELETE /delete file
router.delete('/file', deleteFile);

// DELETE /delete folder
router.delete('/folder', deleteFolder);

export { router as imagesRoute };
