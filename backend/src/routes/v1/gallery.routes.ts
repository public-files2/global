import GalleryController from '../../controllers/gallery.contoller';
import { Router } from 'express';

const router = Router();

// GET / - Retrieve all Gallery
router.get('/', GalleryController.getAllGallery);

// GET /:id - Retrieve Gallery By id
router.get('/:id', GalleryController.getGalleryById);

// POST / - Create a new Gallery
router.post('/', GalleryController.createGallery);

// PATCH /id - Update an existing Gallery
router.patch('/:id', GalleryController.updateGallery);

// PATCH /id - Update an existing Gallery image priority
router.patch('/priority/:id', GalleryController.updateGallery);

// DELETE /id - Delete an existing Gallery
router.delete('/:id', GalleryController.deleteGallery);

export { router as galleryRoute };
