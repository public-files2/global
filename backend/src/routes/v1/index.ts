import express from 'express';
import { usersRoute } from './user.routes';
import { galleryRoute } from './gallery.routes';
import { imagesRoute } from './images.routes';
import { uploadFileRoute } from './uploder.routes';
import { folderRoute } from './folders.routes';
import { formRoute } from './formGenerator.routes';
import { postRoute } from './post.routes';

const routes = express.Router();

// Routes
routes.use('/v1/users', usersRoute);
routes.use('/v1/fileuploader', uploadFileRoute);
routes.use('/v1/users', usersRoute);
routes.use('/v1/image', imagesRoute);
routes.use('/v1/media', galleryRoute);
routes.use('/v1/folders', folderRoute);
routes.use('/v1/generateform', formRoute);
routes.use('/v1/posts', postRoute);

export default routes;
