import { Router } from 'express';
import {
   deleteById,
   getAll,
   getById,
   loginUser,
   registerUser,
   sendOTP,
   updateById,
} from '../../controllers/users.controller';
import requireFieldsMiddleware from '../../middlewares/requireFieldsMiddleware';
const router = Router();

// GET /Users/
router.get('/', getAll);

// GET /Users/:id
router.get('/:id', getById);

// POST / registerUsers
router.post('/', requireFieldsMiddleware(['userName', 'password']), registerUser);

// POST /registration otp
router.post('/otp', requireFieldsMiddleware(['userName', 'password']), sendOTP);

// POST /loginUsers
router.post('/login', requireFieldsMiddleware(['userName', 'password']), loginUser);

// PATCH /Users/:id
router.patch('/:id', updateById);

router.delete('/:id', deleteById);

export { router as usersRoute };
