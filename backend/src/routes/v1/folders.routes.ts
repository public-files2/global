import FolderController from '../../controllers/folders.contoller';
import { Router } from 'express';

const router = Router();

// GET / - Retrieve all Folder
router.get('/', FolderController.getAllFolder);

// GET /:id - Retrieve Folder By id
router.get('/:id', FolderController.getFolderById);

// POST / - Create a new Folder
router.post('/', FolderController.createFolder);

// PATCH /id - Update an existing Folder
router.patch('/:id', FolderController.updateFolder);

// PATCH /id - Update an existing Folder image priority
router.patch('/priority/:id', FolderController.updateFolder);

// DELETE /id - Delete an existing Folder
router.delete('/:id', FolderController.deleteFolder);

export { router as folderRoute };
