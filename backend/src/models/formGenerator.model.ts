import mongoose, { Document, Schema } from 'mongoose';

export interface IForm extends Document {
   formName: string;
   formDetails: Array<{
      label: string;
      inputType: string;
      valueTitle: string;
      valueName: string;
      gridNumber: string;
   }>;
   userId: string;
}

const FormSchema: Schema = new Schema(
   {
      formName: { type: String },
      formDetails: [{
         label: { type: String },
         inputType: { type: String },
         valueTitle: { type: String },
         valueName: { type: String },
         gridNumber: { type: String },
      }],

      userId: { type: String },
   },
   { timestamps: true }
);

export const FormModel = mongoose.model<IForm>('Form', FormSchema);