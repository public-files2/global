import mongoose, { Document, Schema } from 'mongoose';

export interface IGallery extends Document {
   _id: string;
   title: string;
   type: string;
   category: string;
   url: string;
   fileType: string;
   folderId: string;
   isPopular: boolean;
   priority: number;
   userId: string;
   active: boolean;
}

const GallerySchema: Schema = new Schema(
   {
      title: { type: String },
      type: { type: String },
      category: {
         type: String,
      },
      url: { type: String },
      fileType: {
         type: String,
         enum: ['IMAGE', 'VIDEO'],
         default: 'IMAGE',
      },
      folderId: { type: String },
      image: { type: String },
      isPopular: { type: Boolean, default: false },
      priority: { type: Number },
      userId: { type: String, ref: "User" },
      active: { type: Boolean, default: true },
   },
   { timestamps: true }
);

export const GalleryModel = mongoose.model<IGallery>('gallery', GallerySchema);
