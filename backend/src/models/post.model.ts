import mongoose, { Document, Schema } from 'mongoose';

export interface IPost extends Document {
   title: string;
   category: string;
   url: string;
   fileType: string;
   isPopular: boolean;
   userId: string;
   likes: string;
   comments: string;
}
const CommentSchema = new Schema(
   {
      userId: { type: Schema.Types.ObjectId, ref: "User" },
      text: { type: String },
   },
   { timestamps: true }
);

const PostSchema: Schema = new Schema<IPost>(
   {
      title: { type: String },
      category: {
         type: String,
      },
      url: { type: String },
      fileType: {
         type: String,
         enum: ['IMAGE', 'VIDEO'],
         default: 'IMAGE',
      },
      isPopular: { type: Boolean, default: false },
      userId: { type: String, ref: "User" },
      likes: [{ type: String, ref: "User" }],
      comments: [CommentSchema],
   },
   { timestamps: true }
);

export const PostModel = mongoose.model<IPost>('post', PostSchema);
