import mongoose, { Document, Schema } from 'mongoose';

export interface IFolder extends Document {
   folderName: string;
   userId: string;
}

const FolderSchema: Schema = new Schema(
   {
      folderName: { type: String },
      userId: { type: String },
   },
   { timestamps: true }
);

export const FolderModel = mongoose.model<IFolder>('folder', FolderSchema);