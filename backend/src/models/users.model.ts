import { Document, model, Schema } from 'mongoose';

export interface IUser extends Document {
   firstName: string;
   lastName: string;
   email: string;
   mobile: string;
   profileImg: string;
   userName: string;
   password: string;
   viewType: string;
   folders: []
}

const userSchema = new Schema<IUser>(
   {
      firstName: { type: String, },
      lastName: { type: String, },
      email: { type: String, },
      mobile: { type: String, },
      profileImg: { type: String, },
      userName: { type: String },
      password: { type: String },
      viewType: { type: String },
      folders: { type: [] }
   },
   { timestamps: true }
);

export default model<IUser>('User', userSchema);
