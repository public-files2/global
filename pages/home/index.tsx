import HomePage from "@/containers/home"

const HomeRoute = () => {
    return (
        <HomePage />
    )
}
export default HomeRoute